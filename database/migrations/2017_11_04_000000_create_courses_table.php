<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title',150);
            $table->text('excerpt',150);
            $table->text('description');
            $table->string('price', 25);
            $table->text('requirement');
            $table->text('audience');
            $table->text('what_i_get');
            $table->text('course_overview');
            $table->string('image',50)->nullable();
            $table->string('slug',200)->unique();
            $table->text('meta_description');
            $table->text('meta_keywords');
            $table->enum('status', ['PUBLISHED', 'DRAFT', 'PENDING'])->default('DRAFT');
            $table->boolean('featured')->default(0);

            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('sub_categories')->onUpdate('cascade')->onDelete('cascade');

            $table->integer('course_count_reviews');
            $table->integer('course_rating');
            $table->integer('course_view_count');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses');
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('title');
            $table->longText('content');
            $table->string('slug',200)->unique();
            $table->string('resource')->nullable(); //is video then id video
            $table->string('download')->nullable(); //file for download
            $table->boolean('type')->default(1); //type lessons 1 is video, 0 is letter

            $table->integer('module_id')->unsigned();
            $table->foreign('module_id')->references('id')->on('course_modules');

            $table->integer('course_id')->unsigned();
            $table->foreign('course_id')->references('id')->on('courses');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lessons');
    }

}

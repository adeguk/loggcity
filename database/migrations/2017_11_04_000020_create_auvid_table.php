<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuvidTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auvids', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('title');
            $table->string('slug', 200)->unique();
            $table->string('type');
            $table->string('link')->nullable();
            $table->string('youtube_link')->nullable();
            $table->string('order_list');

            $table->integer('lesson_id')->unsigned();
            $table->foreign('lesson_id')->references('id')->on('lessons');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('auvids');
    }

}

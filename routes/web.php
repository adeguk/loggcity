<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $posts = App\Models\Post::take(3)->get();
    return view('ile')->with('posts', $posts);
});

Route::get('/googleeabd2960763467b9.html', function () {
    return \File::get('/googleeabd2960763467b9.html');
});

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/home', 'HomeController@index')->name('home');

// Pages
Route::get('/in-brief', 'PagesController@inBrief');
//Route::get('/our-divisions', 'PagesController@ourDivisions');
//Route::get('/iknow-loggcity-academy', 'PagesController@iknow')->name('iknow');
Route::get('/getting-in-touch', 'PagesController@contact');
Route::post('/send_contact_mail', 'PagesController@sendContactMail')->name('pages.sendContactMail');

// Services
Route::get('/what-we-do', 'ServicesController@whatWeDo');
Route::get('/it-service', 'ServicesController@itServices');
Route::get('/it-support', 'ServicesController@itSupport');

// FAQ
Route::get('/frequently-asked-question', 'FaqsController@index')->name('faq');

// Company Blog
Route::get('/blog', 'PostsController@index');
Route::get('/company-blog/{slug}', 'PostsController@show')->name('posts.show');
Route::get('/blog/feeds', 'PostsController@feed')->name('posts.feed');              // Display Blog RSS

// Company Blog
Route::get('/courses', 'CoursesController@index');
Route::get('/courses/{slug}', 'CoursesController@show')->name('courses.show');

// Newsletter
Route::resource('newsletter-subscriptions', 'NewsletterSubscriptionsController', ['only' => 'store']);
Route::get('newsletter-subscriptions/unsubscribe', 'NewsletterSubscriptionsController@unsubscribe')->name('newsletter-subscriptions.unsubscribe');


// show generic pages create via CRUD
Route::get('/{slug}', 'PagesController@show')->name('pages.show');

Route::get(public_path() . '/googleeabd2960763467b9.html');


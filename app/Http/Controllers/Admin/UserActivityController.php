<?php

namespace App\Http\Controllers\Admin;

use App\Models\UserActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

class UserActivityController extends Controller
{
    /**
     * Display a listing of UserActivity.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('user_activity_access')) {
            return abort(401);
        }

        $user_activities = UserActivity::all();

        return view('admin.users.user_activity', compact('user_activities'));
    }
}

                
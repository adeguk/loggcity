<?php

namespace App\Http\Controllers\Admin;

use App\Models\Company;
use App\Models\CompanyContact;
use App\Models\UserActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreContactsRequest;

class CompanyContactsController extends Controller
{
    /**
     * Display a listing of Contact.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $my_activities = UserActivity::where('user_id',$user_id)->latest('created_at')->limit(3)->get();

        if (! Gate::allows('contact_access')) {
            return abort(401);
        }

        $companies = Company::get()->pluck('name', 'id')->prepend(trans('admin.qa_please_select').' Company', '');
        $contacts = CompanyContact::all();

        return view('admin.contact_manager.contacts', compact('contacts', 'companies', 'my_activities'));
    }

    /**
     * Store a newly created Contact in storage.
     *
     * @param  \App\Http\Requests\StoreContactsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContactsRequest $request)
    {
        if (! Gate::allows('contact_create')) {
            return abort(401);
        }
        //dd($request->all());
        $contact = CompanyContact::create($request->all());

        return redirect()->route('admin.company_contacts.index');
    }

    /**
     * Show the form for editing Contact.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('contact_edit')) {
            return abort(401);
        }

        $companies = Company::get()->pluck('name', 'id')->prepend(trans('admin.qa_please_select').' Company', '');
        $contact = CompanyContact::findOrFail($id);

        return view('admin.contact_manager.edit_contact', compact('contact', 'companies'));
    }

    /**
     * Update Contact in storage.
     *
     * @param  \App\Http\Requests\UpdateContactsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreContactsRequest $request, $id)
    {
        if (! Gate::allows('contact_edit')) {
            return abort(401);
        }
        $contact = CompanyContact::findOrFail($id);
        $contact->update($request->all());

        return redirect()->route('admin.company_contacts.index');
    }

    /**
     * Remove Contact from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('contact_delete')) {
            return abort(401);
        }
        $contact = CompanyContact::findOrFail($id);
        $contact->delete();

        return redirect()->route('admin.company_contacts.index');
    }

    /**
     * Delete all selected Contact at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('contact_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = CompanyContact::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}


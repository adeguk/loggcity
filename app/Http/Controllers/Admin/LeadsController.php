<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Lead;
use App\Models\UserActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class LeadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $my_activities = UserActivity::where('user_id',$user_id)->latest('created_at')->limit(3)->get();

        if (! Gate::allows('lead_access')) {
            return abort(401);
        }

        $leads = Lead::all();

        return view('admin.sales.leads', compact('leads', 'my_activities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('lead_create')) {
            return abort(401);
        }
        $lead = Lead::create($request->all());

        return redirect()->route('admin.leads.index')->withSuccess(__('users.created'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function edit(Lead $lead)
    {
        if (! Gate::allows('lead_edit')) {
            return abort(401);
        }
        $leadResult = Lead::findOrFail($lead);

        return view('admin.sales.edit_lead', compact('leadResult'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lead $lead)
    {
        if (! Gate::allows('lead_edit')) {
            return abort(401);
        }
        $leadResult = Lead::findOrFail($lead);
        $leadResult->update($request->all());

        return redirect()->route('admin.leads.index')->withSuccess(__('users.created'));
    }

    /**
     * Display Lead.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lead)
    {
        if (! Gate::allows('lead_view')) {
            return abort(401);
        }

        $leadResult = Lead::findOrFail($lead);

        return view('admin.sales.show_lead', compact('leadResult'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('company_delete')) {
            return abort(401);
        }
        $leadResult = Lead::findOrFail($id);
        $leadResult->delete();

        return redirect()->route('admin.leads.index');
    }
}

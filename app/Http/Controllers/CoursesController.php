<?php

namespace App\Http\Controllers;

use App\Models\Course;

class CoursesController extends Controller {
    public function index(){
        $page = array(
            'meta_description' => 'How to contact Loggcity? customer services, by email and phone. ... See all Help and FAQs. 
                If you need to contact us about our web development, digital marketing, Online training or any other of our products and/or services',
            'meta_keywords' => 'training and development, PHP, web development, digital marketing, Online training, Microsoft office, iknow0',
            'title' => 'Course Lists'
        );

        $courses = Course::published()->paginate(12);

        return view('pages.course.index', compact('courses', 'page'));
    }

    public function show($slug){
        $course = Course::findBySlug($slug);

        // Select any 3 related courses at random
        $collection = Course::where('category_id', $course->category_id)->get();
        $related_courses = $collection->random(3);
        $related_courses->all();

        //dd($related_courses);

        return view('pages.course.single')->with([
            'page' => $course,
            'currency_symbol'=> 'N',
            'related_courses' => $related_courses,
        ]);
    }

}
<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

class ServicesController extends Controller
{
    public function whatWeDo()
    {
        $pageData = array(
            'meta_description' => 'We are one-stop solution for IT related services... We provide support, training, 
                        documentation and other on-going after sakes service as key elements of our long-term 
                        relationship with our customer when necessary.',
            'meta_keywords' => 'training, Web Development, Loggcity Limited, Loggcity',
            'title' => 'What We Do'
        );

        return view('pages.services.whatWeDo', ['page'=>$pageData]);
    }

    public function itServices()
    {
        $pageData = array(
            'meta_description' => 'With pool of professionals we provide creative 
                and Web Design, E-Marketing and SEO, E-Commerce Solutions as well as re-investing in R&D to promote business success',
            'meta_keywords' => 'Web Design, E-Marketing, SEO, E-Commerce, IT services, Loggcity Limited, Loggcity digital',
            'title' => 'IT Services'
        );

        return view('pages.services.itServices', ['page'=>$pageData]);
    }

    public function itSupport()
    {
        $pageData = array(
            'meta_description' => 'cost-effective IT support services to hundreds of businesses nationwide, 
                Loggcity typically supports clients from various industrial sector including government, education, 
                banking; and specialise in IT support, web maintenance, ',
            'meta_keywords' => 'service, IT Support, helpdesk, Loggcity Limited, Loggcity',
            'title' => 'IT Support'
        );

        return view('pages.services.itSupport', ['page'=>$pageData]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Support\Facades\Cache;

class PostsController extends Controller {
    /**
     * Show all posts
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $page = array(
            'meta_description' => 'How to contact Loggcity? customer services, by email and phone. ... See all Help and FAQs. 
                If you need to contact us about our web development, digital marketing, Online training or any other of our products and/or services',
            'meta_keywords' => 'News, Presentations, Media Relation, interview, Online training, News Update, Event',
            'title' => 'Media Relation: News & Events'
        );

        $posts = Post::orderby('created_at', 'desc')->get();

        return view('pages.blog.index', compact('posts', 'page'));
    }

    /**
     * Show a single post and related articles based on slug and subcategory.
     *
     * @param $slug
     * @return $this
     */
    public function show($slug){
        $page = Post::findBySlug($slug);
        return view('pages.blog.single')->with([
            'page' => $page,
            'single'=>'class=single-post',
        ]);
    }

    /**
     * Show the rss feed of posts.
     *
     * @return Response
     */
    public function feed()
    {
        $posts = Cache::remember('feed-posts', 60, function () {
            return Post::latest()->limit(20)->get();
        });

        return response()->view('pages.blog.feeds', [
            'posts' => $posts
        ], 200)->header('Content-Type','text/xml');
    }

}
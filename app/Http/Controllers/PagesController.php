<?php

namespace App\Http\Controllers;

use App\Http\Request\ContactRequest;
use App\Models\Page;
use App\Models\Lead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

class PagesController extends Controller
{
    public function show($slug){
        $page = Page::findBySlug($slug);
        return view('pages.page', compact('page'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inBrief(){
        $pageData = array(
            'meta_description' => 'our various divisions, though in different industrial sectors had find a way to connect
                themselves seemlessly in an unusual way; working with relatively similar technologies and end-markets...',
            'meta_keywords' => 'Loggcity Divisions, Loggcity Academy, Lumiverse, Training, Capide, iKnow',
            'title' => 'In Brief'
        );

        return view('pages.about.in_brief', ['page'=>$pageData]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact(){
        $pageData = array(
            'meta_description' => 'How to contact Loggcity? customer services, by email and phone. ... See all Help and FAQs. 
                If you need to contact us about our web development, digital marketing, Online training or any other of our products and/or services',
            'meta_keywords' => 'contact',
            'title' => 'Getting in Touch'
        );

        return view('pages.about.contact', ['page'=>$pageData]);
    }

    /**
     *
     */
    public function sendContactMail(Request $request){
        $subject = $request->input('subject');
        $fullname = $request->input('fullname');
        $company = $request->input('company');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $body = $request->input('body');
        $created_at = Carbon::now();

        $emailExist = Lead::where('email', $email)->first();

        if (! $emailExist && $subject == "General Enquiry"){
                $lead = Lead::create([
                    'name' => $fullname,
                    'company' => $company,
                    'description' => $body,
                    'email' => $email,
                    'phonenumber' => $phone,
                    'dateadded' => $created_at
                ]);
        } 

            Mail::send('emails.contact',
                array(
                    'name' => $fullname,
                    'email' => $email,
                    'company' => $company,
                    'phone' => $phone,
                    'subject' => $subject,
                    'body' => $body
                ),
                function($mail) use ($email, $subject) {
                    $mail->from($email);
                    $mail->to('adeguk@gmail.com')
                        ->subject('Loggcity: A new '.$subject);
                }
            );    
        
        return back()->withSuccess('Message sent successfully. A Loggcity representative will reply you as soon as possible!');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function iknow(){
        $pageData = array(
            'meta_description' => 'Our Educational and Training Centre (ETC) referred to as "iKnow" provide a unique 
                learning and development opportunities for individuals and Corporate bodies nationwide.',
            'meta_keywords' => 'iKnow, Loggcity Academy, Loggcity, Training, PHP',
            'title' => 'iKnow: Loggcity Academy'
        );

        return view('pages.about.iknow', ['page'=>$pageData]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ourDivisions(){
        $pageData = array(
            'meta_description' => 'our various divisions, though in different industrial sectors had find a way to connect
                themselves seemlessly in an unusual way; working with relatively similar technologies and end-markets...',
            'meta_keywords' => 'Loggcity Divisions, Loggcity Academy, Lumiverse, Training, Capide, iKnow',
            'title' => 'Our Divisions'
        );

        return view('pages.about.our_divisions', ['page'=>$pageData]);
    }
}

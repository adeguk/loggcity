<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use App\Models\FaqGrouping;
use Illuminate\Http\Request;

class FaqsController extends Controller {
    public function index(){
        $page = array(
            'meta_description' => 'This is Loggcity\'s list of most frequently asked questions. Here, you see further 
                        information about training academy, your user account, getting support and making payment. If you don\'t find the answer 
                        you\'re looking for you can contact us through the "Help" section of our website.',
            'meta_keywords' => 'consultant, FAQ, frequently asked question, Loggcity Limited, Loggcity',
            'title' => 'FAQs'
        );

        $faqs = Faq::all();
        return view('pages.faq', compact('faqs', 'page'));
    }
}
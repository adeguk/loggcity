<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyContact extends Model
{
    protected $fillable = ['first_name', 'last_name', 'work_phone1', 'personal_phone1', 'email', 'skype', 'address', 'company_id'];

    public static function boot()
    {
        parent::boot();

        CompanyContact::observe(new \App\Observers\UserActivitiesObserver);
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setCompanyIdAttribute($input)
    {
        $this->attributes['company_id'] = $input ? $input : null;
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
}

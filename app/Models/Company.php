<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['name', 'address', 'website', 'email'];

    public static function boot()
    {
        parent::boot();

        Company::observe(new \App\Observers\UserActivitiesObserver);
    }
}



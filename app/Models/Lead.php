<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    protected $fillable = [
        'name',
        'phonenumber',
        'email',
        'company',
        'description',
        'dateadded',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tblleads';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'dateadded'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot() {
        parent::boot();

        Lead::observe(new \App\Observers\UserActivitiesObserver);
    }

    /**
     * Scope a query to only include comments posted last week.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLastWeek($query) {
        return $query->whereBetween('dateadded', [Carbon::now()->subWeek(), Carbon::now()])
            ->latest();
    }

    /**
     * Scope a query to only include comments posted last week.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeThisWeek($query) {
        return $query->whereBetween('dateadded', [Carbon::now()->addWeek(), Carbon::now()])
            ->latest();
    }

    /**
     * Scope a query to order comments by latest posted.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLatest($query) {
        return $query->orderBy('dateadded', 'desc');
    }

}

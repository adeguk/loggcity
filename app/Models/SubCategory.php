<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08/11/2017
 * Time: 00:12
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\Category;
use TCG\Voyager\Traits\Translatable;

class SubCategory extends Model
{
    use Translatable;

    protected $translatable = ['slug', 'name'];

    protected $table = 'sub_categories';

    protected $fillable = ['slug', 'name'];

    public function courses()
    {
        return $this->hasMany(Course::class)
            ->published()
            ->orderBy('created_at', 'DESC');
    }

    public function posts()
    {
        return $this->hasMany(Voyager::modelClass('Post'))
            ->published()
            ->orderBy('created_at', 'DESC');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
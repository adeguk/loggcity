<?php
/**
 * Created by PhpStorm.
 * User: logg
 * Date: 17/09/2017
 * Time: 21:39
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activation extends Model
{
    /**
     * Return the user's activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()  {
        return $this->belongsTo(User::class);
    }
}

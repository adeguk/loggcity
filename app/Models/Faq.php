<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Faq extends Model
{
    public $timestamps = false;

    /**
     * Get the Fag_Go that owns the phone.
     */
    public function faq_group()
    {
        return $this->belongsTo(FaqGrouping::Class, 'faq_group_id');
    }
}

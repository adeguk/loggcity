<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\Category;

class Post extends Model
{
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string {
        if (request()->expectsJson()) {
            return 'id';
        }
        return 'slug';
    }

    /**
     * @param $slug
     * @return mixed
     */
    public static function findBySlug($slug){
        return static::where('slug', $slug)->first();
    }

    /**
     * Scope a query to order posts by latest posted
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLatest($query) {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Get the Author of Post.
     */
    public function author()
    {
        return $this->belongsTo(User::Class, 'author_id');
    }

    /**
     * Get the Post Category.
     */
    public function category()
    {
        return $this->belongsTo(Category::Class, 'category_id');
    }
}

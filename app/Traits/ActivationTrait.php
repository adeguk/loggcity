<?php
/**
 * Created by PhpStorm.
 * User: logg
 * Date: 17/09/2017
 * Time: 21:32
 */

namespace App\Traits;

use App\Models\User;
use Illuminate\Support\Facades\Validator;
use App\Logic\Activation\ActivationRepository;

trait ActivationTrait
{
    public function initiateEmailActivation(User $user) {
        if ( !config('settings.activation')  || !$this->validateEmail($user)) {
            return true;
        }

        $activationRepository = new ActivationRepository();
        $activationRepository->createTokenAndSendEmail($user);
    }

    protected function validateEmail(User $user) {

        // Check does email posses valid format, cause if it's social account without
        // email, it'll have value of missing xxxxxxxxxx
        $validator = Validator::make(['email' => $user->email], ['email' => 'required|email']);

        if ($validator->fails()) {
            return false; // Stopping job execution, if it return false it will break entire application
        }

        return true;
    }
}
<?php

namespace App\Jobs;

use App\Models\NewsletterSubscription;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\SendNewsletterSubscriptionEmail;
//use Illuminate\Foundation\Bus\DispatchesJobs;

class PrepareNewsletterSubscriptionEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $newsletterSubscriptions = NewsletterSubscription::all();
        $newsletterSubscriptions->each(function ($newsletterSubscription) {
            $this->dispatch(new SendNewsletterSubscriptionEmail($newsletterSubscription->email));
        });
        $this->dispatch((new PrepareNewsletterSubscriptionEmail())->delay(Carbon::now()->addMonth()));
    }
}

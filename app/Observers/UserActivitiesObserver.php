<?php
/**
 * Created by PhpStorm.
 * User: logg
 * Date: 20/09/2017
 * Time: 16:24
 */

namespace App\Observers;

use App\Models\UserActivity;
//use Auth;
use Illuminate\Support\Facades\Auth;

class UserActivitiesObserver
{
    public function saved($model)
    {
        if ($model->wasRecentlyCreated == true) {
            // Data was just created
            $action = 'created';
        } else {
            // Data was updated
            $action = 'updated';
        }

        if (Auth::check()) {
            $user = Auth::user()->id;

            UserActivity::create([
                'user_id'      => $user,
                'action'       => $action,
                'action_model' => $model->getTable(),
                'action_id'    => $model->id
            ]);
        }
    }


    public function deleting($model)
    {
        if (Auth::check()) {
            UserActivity::create([
                'user_id'      => Auth::user()->id,
                'action'       => 'deleted',
                'action_model' => $model->getTable(),
                'action_id'    => $model->id
            ]);
        }
    }
}

                
<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>Loggcity | Responsive Multi-Purpose HTML5 Template</title>
    
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />

    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="description" content="Loggcity | Responsive Multi-Purpose HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Theme Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!--link rel="stylesheet" href="css/font-awesome.min.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,300,500,600,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/animate.min.css"-->

    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.carousel.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.transitions.css') }}" media="screen" />
    <!-- Magnific Popup core CSS file -->
    <link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}"> 

    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/revolution_slider/css/settings.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/revolution_slider/css/style.css') }}" media="screen" />
    
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="{{ asset('css/app.css') }}">
    
    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>
<body>
    <div id="page-wrapper">
        <header id="header" class="header-color-white">
            @include('partials._primary_menu')
            @include('partials._mobile_menu') 
        </header>            
                   
        @include('partials.component.slideshow')

        <section id="content">
            <div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="icon-box style-boxed-1 animated box" data-animation-type="fadeInUp" data-animation-delay="0.5">
                                <div class="icon-container">
                                    <i class="fa fa-leaf"></i>
                                </div>
                                <div class="box-content">
                                    <h4 class="box-title"><a href="#">Genuine Personality</a></h4>
                                    <p>Loggcity is one of the contributors of IT business solution to variety of industry sectors in Africa and Europe.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="icon-box style-boxed-1 animated box" data-animation-type="fadeInUp" data-animation-delay="0.75">
                                <div class="icon-container">
                                    <i class="fa fa-search"></i>
                                </div>
                                <div class="box-content">
                                    <h4 class="box-title"><a href="#">Strong R&amp;D</a></h4>
                                    <p>We offer an integrated service to promote business success and knowledge management in both private and public sector.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="icon-box style-boxed-1 animated box" data-animation-type="fadeInUp" data-animation-delay="1">
                                <div class="icon-container">
                                    <i class="fa fa-tint"></i>
                                </div>
                                <div class="box-content">
                                    <h4 class="box-title"><a href="#">Limitless Possibilities</a></h4>
                                    <p>We strongly believe in the potential growth of any business and we're passionate about working alongside you to fulfil it.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- post slideshow -->
            <div class="post-wrapper">
                <div class="owl-carousel post-slider style6" data-items="4" data-itemsPerDisplayWidth="[[0, 1], [480, 1], [768, 2], [992, 3], [1200, 4]]">
                    <article class="post">
                        <figure><img alt="" src="http://placehold.it/780x520"></figure>
                        <div class="portfolio-hover-holder">
                            <div class="portfolio-text">
                                <div class="portfolio-text-inner">
                                    <h5 class="portfolio-title">Fullwidth Slideshow</h5> - <span class="portfolio-category">Fashion</span>
                                </div>
                            </div>
                            <span class="portfolio-action">
                                <a href="http://placehold.it/780x520" class="soap-mfp-popup"><i class="fa fa-chain has-circle"></i></a>
                                <a href="portfolio-single1.html"><i class="fa fa-eye has-circle"></i></a>
                            </span>
                        </div>
                    </article>
                    <article class="post">
                        <figure><img alt="" src="http://placehold.it/780x520"></figure>
                        <div class="portfolio-hover-holder">
                            <div class="portfolio-text">
                                <div class="portfolio-text-inner">
                                    <h5 class="portfolio-title">Fullwidth Slideshow</h5> - <span class="portfolio-category">Fashion</span>
                                </div>
                            </div>
                            <span class="portfolio-action">
                                <a href="http://placehold.it/780x520" class="soap-mfp-popup"><i class="fa fa-chain has-circle"></i></a>
                                <a href="portfolio-single1.html"><i class="fa fa-eye has-circle"></i></a>
                            </span>
                        </div>
                    </article>
                    <article class="post">
                        <figure><img alt="" src="http://placehold.it/780x520"></figure>
                        <div class="portfolio-hover-holder">
                            <div class="portfolio-text">
                                <div class="portfolio-text-inner">
                                    <h5 class="portfolio-title">Fullwidth Slideshow</h5> - <span class="portfolio-category">Fashion</span>
                                </div>
                            </div>
                            <span class="portfolio-action">
                                <a href="http://placehold.it/780x520" class="soap-mfp-popup"><i class="fa fa-chain has-circle"></i></a>
                                <a href="portfolio-single1.html"><i class="fa fa-eye has-circle"></i></a>
                            </span>
                        </div>
                    </article>
                    <article class="post">
                        <figure><img alt="" src="http://placehold.it/780x520"></figure>
                        <div class="portfolio-hover-holder">
                            <div class="portfolio-text">
                                <div class="portfolio-text-inner">
                                    <h5 class="portfolio-title">Fullwidth Slideshow</h5> - <span class="portfolio-category">Fashion</span>
                                </div>
                            </div>
                            <span class="portfolio-action">
                                <a href="http://placehold.it/780x520" class="soap-mfp-popup"><i class="fa fa-chain has-circle"></i></a>
                                <a href="portfolio-single1.html"><i class="fa fa-eye has-circle"></i></a>
                            </span>
                        </div>
                    </article>
                    <article class="post">
                        <figure><img alt="" src="http://placehold.it/780x520"></figure>
                        <div class="portfolio-hover-holder">
                            <div class="portfolio-text">
                                <div class="portfolio-text-inner">
                                    <h5 class="portfolio-title">Fullwidth Slideshow</h5> - <span class="portfolio-category">Fashion</span>
                                </div>
                            </div>
                            <span class="portfolio-action">
                                <a href="http://placehold.it/780x520" class="soap-mfp-popup"><i class="fa fa-chain has-circle"></i></a>
                                <a href="portfolio-single1.html"><i class="fa fa-eye has-circle"></i></a>
                            </span>
                        </div>
                    </article>
                    <article class="post">
                        <figure><img alt="" src="http://placehold.it/780x520"></figure>
                        <div class="portfolio-hover-holder">
                            <div class="portfolio-text">
                                <div class="portfolio-text-inner">
                                    <h5 class="portfolio-title">Fullwidth Slideshow</h5> - <span class="portfolio-category">Fashion</span>
                                </div>
                            </div>
                            <span class="portfolio-action">
                                <a href="http://placehold.it/780x520" class="soap-mfp-popup"><i class="fa fa-chain has-circle"></i></a>
                                <a href="portfolio-single1.html"><i class="fa fa-eye has-circle"></i></a>
                            </span>
                        </div>
                    </article>
                </div>
                <div class="title-section trend-section">
                    <div class="title-section-wrapper">
                        <div class="container">
                            <p>Amazing Work Showcase</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--div class="section trend-section">
                <div class="container text-center"-->
                    <!--div class="image-container trend-image">                        
                    </div-->
                    <!--div class="heading-box col-md-10 col-lg-8">
                        <h2 class="box-title">IT trends changed our the past year</h2>
                        <br>
                        <p> Many of the web design trends which you saw last year are disappearing because it’s been researched that flat designs with more human centered design approach are in trend and will stay for a longer period of time.</p>
                    </div-->
                    <!--div class="box">&nbsp;</div-->
                    <!--div class="progress-bar-container style-vertical">
                        <div class="progress-bar-wrapper">
                            <div class="progress-bar animate-progress style-colored">
                                <span class="progress-percent"><span>99%</span></span>
                                <div class="progress"><span class="progress-inner" data-percent="99"><span class="progress-label"><span>Responsive designs</span></span></span></div>
                            </div>
                            <div class="progress-bar animate-progress style-colored">
                                <span class="progress-percent"><span>98%</span></span>
                                <div class="progress"><span class="progress-inner" data-percent="98"><span class="progress-label"><span>human centered</span></span></span></div>
                            </div>
                            <div class="progress-bar animate-progress style-colored">
                                <span class="progress-percent"><span>41%</span></span>
                                <div class="progress"><span class="progress-inner" data-percent="41"><span class="progress-label"><span>gredients</span></span></span></div>
                            </div>
                            <div class="progress-bar animate-progress style-colored">
                                <span class="progress-percent"><span>81%</span></span>
                                <div class="progress"><span class="progress-inner" data-percent="81"><span class="progress-label"><span>woocommerce</span></span></span></div>
                            </div>
                            <div class="progress-bar animate-progress style-colored">
                                <span class="progress-percent"><span>58%</span></span>
                                <div class="progress"><span class="progress-inner" data-percent="58"><span class="progress-label"><span>jquery</span></span></span></div>
                            </div>
                            <div class="progress-bar animate-progress style-colored">
                                <span class="progress-percent"><span>79%</span></span>
                                <div class="progress"><span class="progress-inner" data-percent="79"><span class="progress-label"><span>one page</span></span></span></div>
                            </div>
                            <div class="progress-bar animate-progress style-colored">
                                <span class="progress-percent"><span>95%</span></span>
                                <div class="progress"><span class="progress-inner" data-percent="95"><span class="progress-label"><span>mobile apps</span></span></span></div>
                            </div>
                            <div class="progress-bar animate-progress style-colored">
                                <span class="progress-percent"><span>59%</span></span>
                                <div class="progress"><span class="progress-inner" data-percent="59"><span class="progress-label"><span>portfolio</span></span></span></div>
                            </div>
                            <div class="progress-bar animate-progress style-colored">
                                <span class="progress-percent"><span>90%</span></span>
                                <div class="progress"><span class="progress-inner" data-percent="90"><span class="progress-label"><span>Retina graphics</span></span></span></div>
                            </div>
                            <div class="progress-bar animate-progress style-colored">
                                <span class="progress-percent"><span>63%</span></span>
                                <div class="progress"><span class="progress-inner" data-percent="63"><span class="progress-label"><span>user interface</span></span></span></div>
                            </div>
                            <div class="progress-bar animate-progress style-colored">
                                <span class="progress-percent"><span>81%</span></span>
                                <div class="progress"><span class="progress-inner" data-percent="81"><span class="progress-label"><span>wordpress</span></span></span></div>
                            </div>
                            <div class="progress-bar animate-progress style-colored">
                                <span class="progress-percent"><span>99%</span></span>
                                <div class="progress"><span class="progress-inner" data-percent="99"><span class="progress-label"><span>Flat designs</span></span></span></div>
                            </div>
                        </div>
                    </div-->
                <!--/div>
            </div-->
            <div class="section">
                <div class="container">
                    <div class="heading-box">
                        <h2 class="box-title">Latest News From Our Blog</h2>
                        <p class="desc-lg">What all the fuzz is about?</p>
                    </div>
                    <div class="row blog-posts">
                        <div class="col-sm-4">
                            <article class="post post-masonry">
                                <div class="post-image">
                                    <div class="image">
                                        <img src="http://placehold.it/780x390" alt="">
                                        <div class="image-extras">
                                            <a href="#" class="post-gallery"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-content">
                                    <div class="post-author">
                                        <a href="#"><img src="http://placehold.it/100x100" alt=""></a>
                                    </div>
                                    <div class="post-meta">
                                        <span class="entry-author fn">By <a href="#">Admin</a></span>
                                        <span class="entry-time"><span class="updated no-display">2014-09-09T15:57:08+00:00</span><span class="published">12 Nov, 2014</span></span>
                                        <span class="post-category">in <a href="#">Web Design</a></span>
                                    </div>
                                    <h3 class="entry-title"><a href="#">Sed faucibus tristique placerat</a></h3>
                                    <p>Aliquam hendrerit a augue insuscipit. Pellentesque id erat quis sapienissim sollicitudin. Nulla mattis rsitmet dolor sollicitudin aliquam.</p>
                                </div>
                                <div class="post-action">
                                    <a href="#" class="btn btn-sm style3 post-comment"><i class="fa fa-comment"></i>25</a>
                                    <a href="#" class="btn btn-sm style3 post-like"><i class="fa fa-heart"></i>480</a>
                                    <a href="#" class="btn btn-sm style3 post-share"><i class="fa fa-share"></i>Share</a>
                                    <a href="#" class="btn btn-sm style3 post-read-more">More</a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-4">
                            <article class="post post-masonry">
                                <div class="post-image">
                                    <div class="image">
                                        <img src="http://placehold.it/780x390" alt="">
                                        <div class="image-extras">
                                            <a href="#" class="post-gallery"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="post-content">
                                    <div class="post-author">
                                        <a href="#"><img src="http://placehold.it/100x100" alt=""></a>
                                    </div>
                                    <div class="post-meta">
                                        <span class="entry-author fn">By <a href="#">Admin</a></span>
                                        <span class="entry-time"><span class="updated no-display">2014-09-09T15:57:08+00:00</span><span class="published">12 Nov, 2014</span></span>
                                        <span class="post-category">in <a href="#">Web Design</a></span>
                                    </div>
                                    <h3 class="entry-title"><a href="#">Enean tempor tincidunt odio</a></h3>
                                    <p>Aliquam hendrerit a augue insuscipit. Pellentesque id erat quis sapienissim sollicitudin. Nulla mattis rsitmet dolor sollicitudin aliquam.</p>
                                </div>
                                <div class="post-action">
                                    <a href="#" class="btn btn-sm style3 post-comment"><i class="fa fa-comment"></i>25</a>
                                    <a href="#" class="btn btn-sm style3 post-like"><i class="fa fa-heart"></i>480</a>
                                    <a href="#" class="btn btn-sm style3 post-share"><i class="fa fa-share"></i>Share</a>
                                    <a href="#" class="btn btn-sm style3 post-read-more">More</a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-4">
                            <article class="post post-masonry">
                                <div class="post-slider style3 owl-carousel">
                                    <img src="http://placehold.it/780x390" alt="">
                                    <img src="http://placehold.it/780x390" alt="">
                                    <img src="http://placehold.it/780x390" alt="">
                                </div>
                                <div class="post-content">
                                    <div class="post-author">
                                        <a href="#"><img alt="" src="http://placehold.it/100x100"></a>
                                    </div>
                                    <div class="post-meta">
                                        <span class="entry-author fn">By <a href="#">Admin</a></span>
                                        <span class="entry-time"><span class="updated no-display">2014-09-09T15:57:08+00:00</span><span class="published">12 Nov, 2014</span></span>
                                        <span class="post-category">in <a href="#">Web Design</a></span>
                                    </div>
                                    <h3 class="entry-title"><a href="#">Dolor tempor diamos</a></h3>
                                    <p>Aliquam hendrerit a augue insuscipit. Pellentesque id erat quis sapienissim sollicitudin. Nulla mattis rsitmet dolor sollicitudin aliquam.</p>
                                </div>
                                <div class="post-action">
                                    <a class="btn btn-sm style3 post-comment" href="#"><i class="fa fa-comment"></i>25</a>
                                    <a class="btn btn-sm style3 post-like" href="#"><i class="fa fa-heart"></i>480</a>
                                    <a class="btn btn-sm style3 post-share" href="#"><i class="fa fa-share"></i>Share</a>
                                    <a class="btn btn-sm style3 post-read-more" href="#">More</a>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
            <div class="parallax colors-section parallax-image1" data-stellar-background-ratio="0.5">
                <div class="section">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-5 col-md-4 animated" data-animation-type="fadeInLeft">
                                <span class="logo-icon"><span class="logo-icon-inner"><img src="{{ asset('images/blank.png') }}" alt=""></span></span>
                            </div>
                            <div class="col-sm-7 pull-right animated" data-animation-type="fadeInRight">
                                <div class="heading-box">
                                    <h2 class="box-title color-white">Loggcity Comes with Unlimited Colorschemes</h2>
                                    <p>Proin et convallis nulla. In lacus velit, venenatis at volutpat congue, sollicitudin ut lorem. Pellentesque blandit libero ac eleifend tempor. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris lectus felis congue ut convallis ac, tempor sit amet urna. </p>
                                </div>
                                <div class="colors-container">
                                    <a href="#" class="skin-color-navy"><span></span></a>
                                    <a href="#" class="skin-color-red"><span></span></a>
                                    <a href="#" class="skin-color-sea"><span></span></a>
                                    <a href="#" class="skin-color-purple"><span></span></a>
                                    <a href="#" class="skin-color-blue"><span></span></a>
                                    <a href="#" class="skin-color-green"><span></span></a>
                                    <a href="#" class="skin-color-gold"><span></span></a>
                                    <a href="#" class="skin-color-gray"><span></span></a>
                                </div>
                                <div class="">
                                    <a class="btn style4" href="#">know more</a>
                                    <a class="btn style4" href="#">purchase</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="callout-box style3">
                <div class="container">
                    <div class="callout-content">
                        <div class="callout-text">
                            <h2>Loggcity will make you fall in love again!</h2>
                        </div>
                        <div class="callout-action">
                            <a class="btn style3" href="#">Purchase Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="parallax has-caption parallax-image2" data-stellar-background-ratio="0.5">
                <div class="caption-wrapper">
                    <h2 class="caption animated size-lg" data-animation-type="fadeInLeft" data-animation-duration="2" data-animation-delay="0">Whatever You Do Give Your Best!</h2>
                    <br>
                    <h3 class="caption animated size-md" data-animation-type="fadeInLeft" data-animation-duration="2" data-animation-delay="1">welcome to parallax scrolling</h3>
                </div>
            </div>
            <div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="icon-box style-side-1 box animated" data-animation-type="fadeInUp" data-animation-delay="0">
                                <i class="fa fa-eye"></i>
                                <div class="box-content">
                                    <h4 class="box-title"><a href="#">Retina Ready</a></h4>
                                    <p>Class aptent taciti sociosqu ad litora torqent conubia nostra, per inceptos himenaeos.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="icon-box style-side-1 box animated" data-animation-type="fadeInUp" data-animation-delay="0.25">
                                <i class="fa fa-laptop"></i>
                                <div class="box-content">
                                    <h4 class="box-title"><a href="#">Multi-Purpose</a></h4>
                                    <p>Class aptent taciti sociosqu ad litora torqent conubia nostra, per inceptos himenaeos.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="icon-box style-side-1 box animated" data-animation-type="fadeInUp" data-animation-delay="0.5">
                                <i class="fa fa-shopping-cart"></i>
                                <div class="box-content">
                                    <h4 class="box-title"><a href="#">WooCommerce Ready</a></h4>
                                    <p>Class aptent taciti sociosqu ad litora torqent conubia nostra, per inceptos himenaeos.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="icon-box style-side-1 box animated" data-animation-type="fadeInUp" data-animation-delay="0.75">
                                <i class="fa fa-umbrella"></i>
                                <div class="box-content">
                                    <h4 class="box-title"><a href="#">Awesome Support</a></h4>
                                    <p>Class aptent taciti sociosqu ad litora torqent conubia nostra, per inceptos himenaeos.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="icon-box style-side-1 box animated" data-animation-type="fadeInUp" data-animation-delay="1">
                                <i class="fa fa-thumbs-o-up"></i>
                                <div class="box-content">
                                    <h4 class="box-title"><a href="#">Modern Design</a></h4>
                                    <p>Class aptent taciti sociosqu ad litora torqent conubia nostra, per inceptos himenaeos.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="icon-box style-side-1 box animated" data-animation-type="fadeInUp" data-animation-delay="1.25">
                                <i class="fa fa-star"></i>
                                <div class="box-content">
                                    <h4 class="box-title"><a href="#">Weekly Updates</a></h4>
                                    <p>Class aptent taciti sociosqu ad litora torqent conubia nostra, per inceptos himenaeos.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="responsive-section">
                <div data-stellar-background-ratio="0.5" class="callout-box style1 parallax parallax-image1">
                    <div class="callout-box-wrapper no-bg">
                        <div class="container">
                            <div class="row same-height">
                                <div class="col-md-6 callout-content-container animated" data-animation-type="fadeInLeft">
                                    <div class="callout-content">
                                        <div class="heading-box">
                                            <h2 class="box-title color-white">Loggcity is Responsive Wordpress Theme</h2>
                                            <p>Mircale is a Hand Crafted Pexil Perfect - Responsive - Multi-Purpose & Retina Ready Premium Wordpress Theme which sets new standards for the web design in 2014.</p>
                                            <p>Each and every aspect / part of the theme has been designed for Retina Ready and Pixel Perfect graphics / display so you  get extremely clear visibility even on different mobile devices which makes Loggcity a truly responsive theme.</p>
                                        </div>
                                        <div class="responsive-button">
                                            <a href="#" class="btn-tablet-view"><i class="fa fa-tablet"></i>Tablet View</a><a href="#" class="btn-mobile-view active"><i class="fa fa-mobile"></i>Mobile View</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 animated" data-animation-type="fadeInRight">
                                    <div class="callout-image-container">
                                        <div class="callout-image hide-children">
                                            <img class="responsive-tablet" alt="" src="{{ asset('images/pages/homepage/1/ipad.png') }}">
											<img class="responsive-mobile active" alt="" src="{{ asset('images/pages/homepage/1/iphone.png') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section">
                <div class="container">
                    <div class="heading-box">
                        <h2 class="box-title">What Customers Are Saying?</h2>
                        <p class="desc-lg">Hand picked customer feedbacks</p>
                    </div>
                    <div class="testimonials style1 owl-carousel box-lg" data-transitionstyle="fade">
                        <div class="testimonial style1">
                            <div class="testimonial-image">
                                <img src="http://placehold.it/92x92" alt="">
                            </div>
                            <div class="testimonial-content fontsize-lg">
                                So clean and to the point, love it!
                            </div>
                            <div class="testimonial-author">
                                <span class="testimonial-author-name">John Doe</span> - <span class="testimonial-author-job">CEO</span>
                            </div>
                        </div>
                        <div class="testimonial style1">
                            <div class="testimonial-image">
                                <img src="http://placehold.it/92x92" alt="">
                            </div>
                            <div class="testimonial-content fontsize-lg">
                                These guys surprise me everytime!
                            </div>
                            <div class="testimonial-author">
                                <span class="testimonial-author-name">Jessica Martin</span> - <span class="testimonial-author-job">CEO</span>
                            </div>
                        </div>
                        <div class="testimonial style1 box-lg">
                            <div class="testimonial-image">
                                <img src="http://placehold.it/92x92" alt="">
                            </div>
                            <div class="testimonial-content fontsize-lg">
                                Incredibly awesome & clean interface!
                            </div>
                            <div class="testimonial-author">
                                <span class="testimonial-author-name">John Doe</span> - <span class="testimonial-author-job">CEO</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @include('partials.footer')
    </div>
    @include('partials._footscript')
    <!-- Scripts -->
    @stack('script')
</body>
</html>
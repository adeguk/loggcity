@extends('layouts.email')

@section('body')
    <span class="preheader">You have a new customer feedback and/or suggestion from office.</span>
    <table class="main">
      <!-- START MAIN CONTENT AREA -->
      <tr>
        <td class="wrapper">
          <table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>
                <p>Hi there,</p>
                <p>{{ $body}}</p>
                <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                  <tbody>
                    <tr>
                      <td align="left">
                        <table border="0" cellpadding="0" cellspacing="0">
                          <tbody>
                            <tr style="height:25px"> <td>Fullname:</td>
                                <td>{{ $name }}</td> </tr>
                            <tr style="height:25px">
                                <td>Email:</td>
                                <td>{{ $email }}</td>
                             </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <p>&nbsp;</p>
                <p>Kind regards</p>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    <!-- END MAIN CONTENT AREA -->
    </table>
@endsection

@extends('layouts.email')

@section('body')
    <!--Header Section-->
    <table width="740" cellspacing="0" cellpadding="0" bgcolor="#" class="100p">
        <tr>
            <td background="images/header-bg.jpg" bgcolor="#1f242e" width="740" valign="top" class="100p">
                <!--[if gte mso 9]>
                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:740px;">
                    <v:fill type="tile" src="images/header-bg.jpg" color="#3b464e" />
                    <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                <![endif]-->
                <div>
                    <table width="740" border="0" cellspacing="0" cellpadding="20" class="100p">
                        <tr>
                            <td valign="top">
                                <table border="0" cellspacing="0" cellpadding="0" width="700" class="100p">
                                    <tr>
                                        <td height="35"></td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="color:#54a4b0; font-size:24px;font-weight: 700;">
                                            <font face="Arial, sans-serif">
                                                <span style="font-size:41px;">Ding-Dong! We've Got Something For You.</span><br />
                                            </font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="35"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--[if gte mso 9]>
                </v:textbox>
                </v:rect>
                <![endif]-->
            </td>
        </tr>
    </table>
    <!-- Divider-->
    <table width="740" border="0" cellspacing="0" cellpadding="0" bgcolor="#b2d6db" class="100p" height="1">
        <tr>
            <td width="20" bgcolor="#FFFFFF"></td>
            <td align="center" height="1" style="line-height:0px; font-size:1px;">&nbsp;</td>
            <td width="20" bgcolor="#ffffff"></td>
        </tr>
    </table>
    <!-- Content: Thanks-->
    <table width="740" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" class="100p">
        <tr>
            <td align="center" style="font-size:16px; color:#848484;line-height: 1.75">
                <font face="'Roboto', Arial, sans-serif"><span style="font-size:16px;">{!! __('newsletter.email.thanks')  !!}</span><br />
                    <br />
                    <span style="color:#2a8e9d; font-size:24px;line-height: normal">{!! __('newsletter.email.appreciation')  !!}</span>
                </font>
            </td>
        </tr>
    </table>
    <!-- Content: highlight-->
    <table width="740" border="0" cellspacing="0" cellpadding="20" bgcolor="#2a8e9d" class="100p">
        <tr>
            <td align="left" style="font-size:20px; color:#FFFFFF;">
                <font face="'Roboto', Arial, sans-serif">
                    For your records, here is a copy of the information you submitted to us...
                    <ul>
                        <li style="list-style: none;"><strong>Email Address :</strong> {{$email}}</li>
                    </ul>
                </font>
            </td>
        </tr>
    </table>

@endsection
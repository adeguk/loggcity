<div class="mobile-nav-wrapper collapse visible-mobile" id="mobile-nav-wrapper">
        <ul class="mobile-nav">
            <li class="">
                <a href="{{URL('/')}}"><i class="fa fa-home"></i></a>
            </li>
            <li class="menu-item-has-children">
                <span class="open-subnav"></span>
                <a href="#">Services</a>
                <ul class="sub-nav">
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Support Solutions</a>
                        <ul class="sub-nav">
                            <li><a href="javascript:void(0)"><i class="fa fa-wrench"></i><span>IT Support</span></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-spinner"></i><span>Business Continuity</span></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i><span>GDPR Consultation</span></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-truck"></i><span>IT Relocation</span></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-desktop"></i><span>Application Infrastruture</span></a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Business Solutions</a>
                        <ul class="sub-nav">
                            <li><a href="javascript:void(0)"><i class="fa fa-users"></i><span>IT Consultancy</span></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-exchange"></i><span>Recruitment &amp; Outsourcing</span></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-tasks"></i><span>Strategic Management</span></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-tags"></i><span>Product Supply</span></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-th-list"></i><span>Project Management</span></a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Divisonal Services</a>
                        <ul class="sub-nav">
                            <li><a href="javascript:void(0)"><i class="fa fa-th"></i><span>Construction Management</span></a></li>
                            <li><a href="https://digital.loggcity.uk"><i class="fa fa-code"></i><span>Digital Services</span></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-leaf"></i><span>e-Agriculture Implementation</span></a></li>
                            <li><a href="javascript:void(0)"><i class="fa fa-search"></i><span>Research &amp; Development</span></a></li>
                            <li><a href="https://iknow.loggcity.uk"><i class="fa fa-book"></i><span>Training &amp; Development</span></a></li>
                        </ul>
                    </li>
                    <li>
                        <p style="color:#fff">We handle exceptional and complex requirements with confidence and passion -
                            by team of business leaders working across multiple divisions.</p>
                    </li>
                </ul>
            </li>
            <li class="menu-item-has-children">
                <span class="open-subnav"></span>
                <a href="#">Industries</a>
                <ul class="sub-nav">
                    <li><a href="shortcode-post-sliders.html"><i class="fa fa-shopping-cart"></i><span>Consumer &amp; Retail</span></a></li>
                    <li><a href="shortcode-typography.html"><i class="fa fa-th"></i><span>Construction &amp; Real Estate</span></a></li>
                    <li><a href="shortcode-progress-bars.html"><i class="fa fa-book"></i><span>Education</span></a></li>
                    <li><a href="shortcode-pricing-tables.html"><i class="fa fa-credit-card"></i><span>FinTech &amp; Financial</span></a></li>
                    <li><a href="shortcode-style-changer.html"><i class="fa fa-medkit"></i><span>Healthcare</span></a></li>
                    <li><a href="shortcode-tabs.html"><i class="fa fa-truck"></i><span>Logistics</span></a></li>
                    <li><a href="shortcode-team.html"><i class="fa fa-caret-square-o-right"></i><span>Media &amp; Entertainment</span></a></li>
                    <li><a href="shortcode-style-changer.html"><i class="fa fa-anchor"></i><span>Public Sector</span></a></li>
                    <li><a href="shortcode-testimonials.html"><i class="fa fa-user"></i><span>Recruitment</span></a></li>
                    <li><a href="shortcode-tabs.html"><i class="fa fa-desktop"></i><span>Technology</span></a></li>
                </ul>
            </li>
            <li class="menu-item-has-children">
                <span class="open-subnav"></span>
                <a href="#">Community</a>
                <ul class="sub-nav">
                    <li><a href="javascript:void(0)">Emerging Techs</a></li>
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Events</a>
                        <ul class="sub-nav">
                            <li><a href="javascript:void(0)">Code Gladiators</a></li>
                            <li><a href="javascript:void(0)">Kodeous</a></li>
                            <li><a href="javascript:void(0)">All Past Events</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Meet-Ups</a>
                        <ul class="sub-nav">
                            <li><a href="blog-mini-fullwidth.html">Coding 4 Kids</a></li>
                            <li><a href="blog-mini-leftsidebar.html">LaraStudent</a></li>
                            <li><a href="blog-mini-rightsidebar.html">PHP Gods</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Patners</a>
                        <ul class="sub-nav">
                            <li><a href="blog-classic-fullwidth.html">Our Partner</a></li>
                            <li><a href="blog-classic-leftsidebar.html">Become a Partner</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Our Clients</a></li>
                </ul>
            </li>
            <li class="menu-item-has-children">
                <span class="open-subnav"></span>
                <a href="#">Company</a>
                <ul class="sub-nav">
                    <li ><a href="{{route('pages.show', 'in-brief')}}">In Brief</a></li>
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Fancy Wide</a>
                        <ul class="sub-nav">
                            <li><a href="portfolio-fancy-wide-3columns.html">3 Columns</a></li>
                            <li><a href="portfolio-fancy-wide-4columns.html">4 Columns</a></li>
                            <li><a href="portfolio-fancy-wide-5columns.html">5 Columns</a></li>
                            <li><a href="portfolio-fancy-wide-6columns.html">6 Columns</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Grid</a>
                        <ul class="sub-nav">
                            <li><a href="portfolio-grid-2columns.html">2 Columns</a></li>
                            <li><a href="portfolio-grid-3columns.html">3 Columns</a></li>
                            <li><a href="portfolio-grid-4columns.html">4 Columns</a></li>
                            <li><a href="portfolio-grid-5columns.html">5 Columns</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Masonry</a>
                        <ul class="sub-nav">
                            <li><a href="portfolio-masonry1.html">Masonry 1</a></li>
                            <li><a href="portfolio-masonry2.html">Masonry 2</a></li>
                            <li><a href="portfolio-masonry3.html">Masonry Text</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Single</a>
                        <ul class="sub-nav">
                            <li><a href="portfolio-single1.html">Big Slider</a></li>
                            <li><a href="portfolio-single2.html">Fullwidth Gallery</a></li>
                            <li><a href="portfolio-single3.html">Fullwidth Project</a></li>
                            <li><a href="portfolio-single4.html">Small Gallery</a></li>
                            <li><a href="portfolio-single5.html">Small Slider Project</a></li>
                            <li><a href="portfolio-single6.html">Vertical Gallery</a></li>
                            <li><a href="portfolio-single7.html">Vertical Project</a></li>
                            <li><a href="portfolio-single8.html">Video Project</a></li>
                            <li><a href="portfolio-single9.html">Wide Image Project</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="menu-item-has-children">
                <span class="open-subnav"></span>
                <a href="#">Resources</a>
                <ul class="sub-nav">
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Home Pages</a>
                        <ul class="sub-nav">
                            <li><a href="woocommerce-homepage1.html">Homepage 1</a></li>
                            <li><a href="woocommerce-homepage2.html">Homepage 2</a></li>
                            <li><a href="woocommerce-homepage3.html">Homepage 3</a></li>
                            <li><a href="woocommerce-homepage4.html">Homepage 4</a></li>
                            <li><a href="woocommerce-homepage5.html">Homepage 5</a></li>
                            <li><a href="woocommerce-homepage6.html">Homepage 6</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Category Pages</a>
                        <ul class="sub-nav">
                            <li><a href="woocommerce-category-grid.html">Grid Style</a></li>
                            <li><a href="woocommerce-category-list.html">List Style</a></li>
                            <li><a href="woocommerce-category-banner.html">With Banner</a></li>
                            <li><a href="woocommerce-category-big-header.html">Big Header</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Product Pages</a>
                        <ul class="sub-nav">
                            <li><a href="woocommerce-product-detailed.html">Product Detailed</a></li>
                            <li><a href="woocommerce-product-sidebar.html">Product Sidebar</a></li>
                        </ul>
                    </li>
                    <li><a href="woocommerce-product-quickview.html">Quick View Popup</a></li>
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Cart Pages</a>
                        <ul class="sub-nav">
                            <li><a href="woocommerce-shopping-cart.html">Shopping Cart</a></li>
                            <li><a href="woocommerce-checkout.html">Checkout</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <span class="open-subnav"></span>
                        <a href="#">Page Layouts</a>
                        <ul class="sub-nav">
                            <li><a href="woocommerce-layout-fullwidth.html">Fullwidth</a></li>
                            <li><a href="woocommerce-layout-sidebar.html">Sidebar</a></li>
                        </ul>
                    </li>
                    <li><a href="woocommerce-dashboard.html">Dashboard</a></li>
                </ul>
            </li>
        </ul>
    </div>
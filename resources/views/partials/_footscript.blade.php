    <!-- Javascript -->
    <script type="text/javascript" src="{{ asset('js/jquery-2.1.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.noconflict.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/modernizr.2.8.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-ui.1.11.2.min.js') }}"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>

    <!-- Magnific Popup core JS file -->
    <script type="text/javascript" src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script> 
    
    <!-- parallax -->
    <script type="text/javascript" src="{{ asset('js/jquery.stellar.min.js') }}"></script>
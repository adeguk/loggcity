<footer id="footer" class="style4">
   <div class="callout-box style2">
       <div class="container">
           <div class="callout-content">
           <div class="callout-text">
                <h2>True partnership Loggcity will make your business in grow again!</h2>
            </div>
            <div class="callout-action">
                <a class="btn style3" href="{{url('/getting-in-touch')}}">Get in Touch</a>
            </div>
           </div>
       </div>
   </div>
   <div class="footer-wrapper">
       <div class="container">
           <div class="row add-clearfix same-height">
               <div class="col-sm-6 col-md-3">
                   <h5 class="section-title box">Recent Posts</h5>
                   <ul class="recent-posts">
                       <li>
                           <a href="#" class="post-author-avatar"><span><img src="http://placehold.it/50x50" alt=""></span></a>
                           <div class="post-content">
                               <a href="#" class="post-title">Website design trends for 2014</a>
                               <p class="post-meta">By <a href="#">Admin</a>  .  12 Nov, 2014</p>
                           </div>
                       </li>
                       <li>
                           <a href="#" class="post-author-avatar"><span><img src="http://placehold.it/50x50" alt=""></span></a>
                           <div class="post-content">
                               <a href="#" class="post-title">UI experts and modern designs</a>
                               <p class="post-meta">By <a href="#">Admin</a>  .  12 Nov, 2014</p>
                           </div>
                       </li>
                       <li>
                           <a href="#" class="post-author-avatar"><span><img src="http://placehold.it/50x50" alt=""></span></a>
                           <div class="post-content">
                               <a href="#" class="post-title">Mircale is available in wordpress</a>
                               <p class="post-meta">By <a href="#">Admin</a>  .  12 Nov, 2014</p>
                           </div>
                       </li>
                   </ul>
               </div>
               <div class="col-sm-6 col-md-3">
                   <h5 class="section-title box">Operating in Nigeria</h5>
                   <ul class="arrow useful-links">
                       <li><a href="https://marketia.com.ng" target="_blank" data-toggle="tooltip" data-placement="top" title="Online platform for financial and money news update from in Africa">Marketia</a></li>
                       <li><a href="http://www.nigeriatradehub.gov.ng/" data-toggle="tooltip" data-placement="top" title="A trade hub for internation trade facilitation">Nigerian Trade Hub</a></li>
                       <li><a href="http://pebec.gov.ng/" target="_blank"  data-toggle="tooltip" data-placement="top" title="Presidential Enabling Business Environment Council">PEBEC, Nigeria</a></li>
                       <li><a href="https://fmard.gov.ng/" target="_blank" data-toggle="tooltip" data-placement="top" title="Federal Ministry of Agriculture and Rural Development">FMARD, Nigeria</a></li>
                       <li><a href="http://sec.gov.ng/" target="_blank" data-toggle="tooltip" data-placement="top" title="Nigerian Securities and Exchange Commission">Securities and Exchange Commission</a></li>
                       <li><a href="http://www.cpn.gov.ng" target="_blank" data-toggle="tooltip" data-placement="top" title="Computer Professionals [Registration Council] of Nigeria (CPN)">CPN Updates</a></li>
                   </ul>
               </div>
               <div class="col-sm-6 col-md-3">
                   <h5 class="section-title box">Useful Links</h5>
                   <ul class="arrow useful-links">
                       <li><a href="https://www.gov.uk/browse/business" target="_blank" data-toggle="tooltip" data-placement="top" title="Doing Business in UK: Information and advice">Doing Business in UK</a></li>
                       <li><a href="www.fsb.org.uk" data-toggle="tooltip" data-placement="top" title="The Federation of Small Business is the leading political lobby for British small business">Federation of Small Business</a></li>
                       <li><a href="https://www.gov.uk/government/publications" target="_blank"  data-toggle="tooltip" data-placement="top" title="Get access to varieties of publications from the UK Government">UK Govt. Publication</a></li>
                       <li><a href="https://www.ft.com/" target="_blank" data-toggle="tooltip" data-placement="top" title="Up to the minute news, information and analysis">Financial Times</a></li>
                       <li><a href="https://www.wsj.com/" target="_blank" data-toggle="tooltip" data-placement="top" title="The Wall Street Journal is a U.S. business-focused, daily newspaper based in New York City">The Wall Street Journal</a></li>
                       <li><a href="https://www.bcs.org/" target="_blank" data-toggle="tooltip" data-placement="top" title="BCS, The Chartered Institute for IT">British Computer Society</a></li>
                   </ul>
               </div>
               <div class="col-sm-6 col-md-3">
                   <h5 class="section-title box">Loggcity in Brief</h5>
                   <p>A solution-oriented business dedicated to helping SMEs, Government Agencies &amp; Institutions create and execute modern strategies that achieve exceptional growth!</p>
                   <div class="social-icons">
                       <a href="https://twitter.com/InfoLoggcity" class="social-icon"><i class="fa fa-twitter has-circle" data-toggle="tooltip" data-placement="top" title="Twitter"></i></a>
                       <a href="https://facebook.com/Loggcity" class="social-icon"><i class="fa fa-facebook has-circle" data-toggle="tooltip" data-placement="top" title="Facebook"></i></a>
                       <a href="#" class="social-icon"><i class="fa fa-google-plus has-circle" data-toggle="tooltip" data-placement="top" title="GooglePlus"></i></a>
                       <a href="https://www.linkedin.com/company/loggcity/" class="social-icon"><i class="fa fa-linkedin has-circle" data-toggle="tooltip" data-placement="top" title="LinkedIn"></i></a>
                   </div>
                   <a href="{{url('/getting-in-touch')}}" class="btn btn-sm style4">Contact Us</a>
                   <a href="javascript:void(o)" class="btn btn-sm style4">Join Us</a>
                   <a href="#" class="back-to-top"><span></span></a>
               </div>
           </div>
       </div>
   </div>
   <div class="footer-bottom-area">
       <div class="container">
           <div class="copyright-area">
               <nav class="secondary-menu">
                   <ul class="nav nav-pills">
                       <li><a href="{{route('pages.show', 'terms-and-condition')}}" data-toggle="tooltip" data-placement="top" title="By surfing our site, you agreed to our terms of use">Terms of Use</a></li>
                       <li><a href="{{route('pages.show', 'privacy-policy')}}">Privacy Policy</a></li>
                       <li><a href="http://support.loggcity.com.ng" target="_blank">Customer Support</a></li>
                       <li><a href="http://gateway.loggcity.uk/" target="_blank" data-toggle="tooltip" data-placement="top" title="Client's Login">the Gateway</a></li>
                       <li><a href="{{url('/blog')}}" data-toggle="tooltip" data-placement="top" title="Get access to the company's latest news">Media Relation</a></li>
                       <li><a href="javascript:void(0)">Shop</a></li>
                   </ul>
               </nav>
                        @php
                            if (isset($modal))  $modal;
                            if( ! ini_get('date.timezone') )
                                date_default_timezone_set('GMT');
                        @endphp
               <div class="copyright">
                            &copy; {{ config('app.name', 'Loggcity').', '. date('Y') }}. <em>Designed by </em>
                   <a href="http://digital.loggcity.uk">Loggcity Digital</a>
               </div>
           </div>
       </div>
   </div>   
</footer>
    <section id="menu-7">
        <nav class="navbar navbar-dropdown navbar-fixed-top blue-grey darken-4">
            @include('partials._navigation')
        </nav>
    </section>

<section class="mbr-section wrapper blue-grey darken-3" id="bannerhead">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="text-center white-text">
                    <h1 class="header-title font-opensans">{{ $page['title'] or $page->title}}</h1>
                </div>
            </div>
        </div>
        <div class="row hidden-xs" style="position:relative;z-index:15">
            <ul class="breadcrumb">
                <li class="fw4">where we are: </li>
                <li><a href="{{url('/')}}">Home</a></li>
                @if (isset($breadcrumbs) && is_array($breadcrumbs))
                    @foreach ($breadcrumbs as $name => $link)
                        @if ($link == '')
                            <li>{{$name}}</li>
                        @else
                            <li><a href="{{$link}}">{{$name}}</a></li>
                        @endif
                    @endforeach
                @else
                    <li>{{$page->title or $page['title']}}</li>
                @endif
            </ul>
        </div>
    </div>
</section>

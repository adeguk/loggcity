<div class="section banner-color1">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="icon-box style-side-1 box animated" data-animation-type="fadeInUp" data-animation-delay="0">
                    <i class="fa fa-eye"></i>
                    <div class="box-content">
                        <h4 class="box-title"><a href="#">Creative &amp; Entrepreneurship</a></h4>
                        <p>With the energy and support to turn great ideas into a reality, Loggcity is a dynamic start-up ecosystem where new initiatives flourish.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="icon-box style-side-1 box animated" data-animation-type="fadeInUp" data-animation-delay="0.25">
                    <i class="fa fa-laptop"></i>
                    <div class="box-content">
                        <h4 class="box-title"><a href="#">Innovation Culture</a></h4>
                        <p>Both the skilled and unskilled staff have made innovation and technology a priority at Loggcity.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="icon-box style-side-1 box animated" data-animation-type="fadeInUp" data-animation-delay="0.5">
                    <i class="fa fa-shopping-cart"></i>
                    <div class="box-content">
                        <h4 class="box-title"><a href="#">International &amp; Global</a></h4>
                        <p>From Western Africa, Nigeria and London UK, Loggcity connect you to the world of possibilities through  an open trading economy.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="icon-box style-side-1 box animated" data-animation-type="fadeInUp" data-animation-delay="0.75">
                    <i class="fa fa-umbrella"></i>
                    <div class="box-content">
                        <h4 class="box-title"><a href="#">Partnership Mindset</a></h4>
                        <p>Across Loggcity its divisions, a pro-partnership organisation creats a supportive busines climate.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="icon-box style-side-1 box animated" data-animation-type="fadeInUp" data-animation-delay="1">
                    <i class="fa fa-thumbs-o-up"></i>
                    <div class="box-content">
                        <h4 class="box-title"><a href="#">Competitive Result</a></h4>
                        <p>Adding more to our business model, with diversified industrial activities backed up by a solid network of business leaders.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="icon-box style-side-1 box animated" data-animation-type="fadeInUp" data-animation-delay="1.25">
                    <i class="fa fa-star"></i>
                    <div class="box-content">
                        <h4 class="box-title"><a href="#">Talent &amp; Education</a></h4>
                        <p>With top level training and industrial leaders, companies can rely on a pool of talented and committed professionals.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="mbr-section wrapper" id="subscribeForm">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="white-text common-title fw7 font-righteous text-xs-center">Subscription Form</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['route' => 'newsletter-subscriptions.store', 'method' => 'post', 'class' => 'form-horizontal, validate']) !!}
                    <div class="form-group label-floating">
                        <div class="col-md-10 col-xs-12">
                            <label class="control-label" name="Email" for="inputEmail">Subscribe to our monthly Newsletter</label>
                            {!! Form::email('email', null, ['id'=>'focusedInput2', 'class'=>'form-control' ]) !!}
                            <p class="help-block">Enter correct email to subscribe</p>
                        </div>
                        <div class="col-md-2 col-xs-12">
                            {!! Form::submit('Subscribe', ['class' => 'btn btn-raised btn-primary']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
                <p class="fw7 text-xs-center white-text">Subscribe to our newsletter, publication and monthly email updates</p>
            </div>
        </div>
    </div>
</section>
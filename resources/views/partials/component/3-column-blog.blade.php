<div class="section">
    <div class="container">
        <div class="heading-box">
            <h2 class="box-title">Latest From Our Media Relation</h2>
            <p class="desc-lg">Connecting you with great thinker, influencers and world-class decision-makers </p>
        </div>
        <div class="row blog-posts">
        @isset($posts)
            @foreach ($posts as $post)
            <div class="col-sm-4">
                <article class="post post-masonry">
                    <div class="post-image">
                        <div class="image">
                        @isset($post->image)
                            <img src="{{asset('storage/'.$post->image)}}" style="width:100%" alt="{{$post->title}}" title="{{$post->title}}" />
                        @endisset
                        @empty($post->image)
                            <img src="http://placehold.it/860x580" alt="Loggcity limited - {{$post->title}}">
                        @endempty
                            <div class="image-extras">
                                <a href="#" class="post-gallery"></a>
                            </div>
                        </div>
                    </div>
                    <div class="post-content">
                        <div class="post-author">
                            <a href="#"><img src="http://placehold.it/100x100" alt="{{$post->custom_author or $post->author->name}}"></a>
                        </div>
                        <div class="post-meta">
                            <span class="entry-author fn">By <a href="javascript:void(0)">{{$post->custom_author or $post->author->name}}</a></span>
                            <span class="entry-time"><span class="updated no-display">{{date("d/m/y", strtotime($post->modified_at))}}</span><span class="published">{{date("d M, y", strtotime($post->created_at))}}</span></span>
                            <span class="post-category">in <a href="javascript:void(0)">{{$post->category->name or 'Uncategories'}}</a></span>
                        </div>
                        <h3 class="entry-title"><a href="{{route('posts.show', $post)}}">{{$post->article_title}}</a></h3>
                        <p>{{str_limit($post->excerpt, $limit = 100, $end = '...')}}</p>
                    </div>
                    <div class="post-action">
                        <a href="#" class="btn btn-sm style3 post-comment"><i class="fa fa-comment"></i>25</a>
                        <a href="#" class="btn btn-sm style3 post-like"><i class="fa fa-heart"></i>480</a>
                        <a href="#" class="btn btn-sm style3 post-share"><i class="fa fa-share"></i>Share</a>
                    </div>
                </article>
            </div>
            @endforeach
        @endisset
        </div>
    </div>
</div>
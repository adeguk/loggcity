<div class="callout-box style1 parallax parallax-image1" data-stellar-background-ratio="0.5">
    <div class="callout-box-wrapper">
        <div class="container">
            <div class="row same-height">
                <div class="col-sm-8 pull-right callout-content-container">
                    <div class="callout-content">
                        <div class="callout-text">
                            <h2>PEBEC-EBES 60-Day National Action Plan</h2>
                        </div>
                        <div class="callout-action">
                            <a href="http://pebec.gov.ng/wp-content/uploads/2017/10/National-Action-Plan-2.0-2017-2018-Reform-Cycle-1.pdf" class="btn style4">know more</a>
                            <a href="http://pebec.gov.ng/wp-content/uploads/2017/10/National-Action-Plan-2.0-2017-2018-Reform-Cycle-1.pdf" class="btn style4">download</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="callout-image-container">
                        <div class="callout-image">
                            <img src="{{ asset('images/slider/ease-of-doing-business.jpg') }}" alt="PEBEC-EBES 60-Day National Action Plan" width="351" height="298">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container callout-stripe-container">
            <div class="callout-stripe">
                Loggcity Beyond the game: Discover the hidden highlights and thriving guide of doing business in various country with this guide.
            </div>
        </div>
    </div>
</div>
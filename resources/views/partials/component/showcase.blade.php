<div class="post-wrapper">
    <div class="owl-carousel post-slider style6" data-items="4" data-itemsPerDisplayWidth="[[0, 1], [480, 1], [768, 2], [992, 3], [1200, 4]]">
        <article class="post">
            <figure><img alt="" src="{{ asset('images/slider/showcase/branding-for-loggcity-digital.jpg') }}" alt="Loggcity Digital"></figure>
            <div class="portfolio-hover-holder">
                <div class="portfolio-text">
                    <div class="portfolio-text-inner">
                        <h5 class="portfolio-title">Loggcity Digital</h5> - <span class="portfolio-category">Branding, IT Support</span>
                    </div>
                </div>
                <span class="portfolio-action">
                    <a href="{{ asset('images/slider/showcase/branding-for-loggcity-digital.jpg') }}" class="soap-mfp-popup"><i class="fa fa-chain has-circle"></i></a>
                    <a href="https://digital.loggcity.uk"><i class="fa fa-eye has-circle"></i></a>
                </span>
            </div>
        </article>
        <article class="post">
            <figure><img alt="" src="{{ asset('images/slider/showcase/digital-marketing-ppc-advertising.jpg') }}" alt="Emergency Glazing 24HR"></figure>
            <div class="portfolio-hover-holder">
                <div class="portfolio-text">
                    <div class="portfolio-text-inner">
                        <h5 class="portfolio-title">Emergency Glazing 24HR</h5> - <span class="portfolio-category">Web, Digital Marketing, PPC advertising</span>
                    </div>
                </div>
                <span class="portfolio-action">
                    <a href="{{ asset('images/slider/showcase/digital-marketing-ppc-advertising.jpg') }}" class="soap-mfp-popup"><i class="fa fa-chain has-circle"></i></a>
                    <a href="https://emergencyglazing24hr.co.uk"><i class="fa fa-eye has-circle"></i></a>
                </span>
            </div>
        </article>
        <article class="post">
            <figure><img alt="" src="{{ asset('images/slider/showcase/jeha-limited.jpg') }}" alt="jeha-limited"></figure>
            <div class="portfolio-hover-holder">
                <div class="portfolio-text">
                    <div class="portfolio-text-inner">
                        <h5 class="portfolio-title">Jeha Limited</h5> - <span class="portfolio-category">Web</span>
                    </div>
                </div>
                <span class="portfolio-action">
                    <a href="{{ asset('images/slider/showcase/jeha-limited.jpg') }}" class="soap-mfp-popup"><i class="fa fa-chain has-circle"></i></a>
                    <a href="https://jeha.co.uk"><i class="fa fa-eye has-circle"></i></a>
                </span>
            </div>
        </article>
        <article class="post">
            <figure><img alt="" src="http://placehold.it/780x520"></figure>
            <div class="portfolio-hover-holder">
                <div class="portfolio-text">
                    <div class="portfolio-text-inner">
                        <h5 class="portfolio-title">Fullwidth Slideshow</h5> - <span class="portfolio-category">Fashion</span>
                    </div>
                </div>
                <span class="portfolio-action">
                    <a href="http://placehold.it/780x520" class="soap-mfp-popup"><i class="fa fa-chain has-circle"></i></a>
                    <a href="portfolio-single1.html"><i class="fa fa-eye has-circle"></i></a>
                </span>
            </div>
        </article>
        <article class="post">
            <figure><img alt="" src="{{ asset('images/slider/showcase/market-concept.jpg') }}" alt="marketia market-concept"></figure>
            <div class="portfolio-hover-holder">
                <div class="portfolio-text">
                    <div class="portfolio-text-inner">
                        <h5 class="portfolio-title">Marketia</h5> - <span class="portfolio-category">Web, Support Solution</span>
                    </div>
                </div>
                <span class="portfolio-action">
                    <a href="{{ asset('images/slider/showcase/market-concept.jpg') }}" class="soap-mfp-popup"><i class="fa fa-chain has-circle"></i></a>
                    <a href="https://marketia.com.ng"><i class="fa fa-eye has-circle"></i></a>
                </span>
            </div>
        </article>
    </div>
    <div class="title-section" style="background-color:#edf6ff">
        <div class="title-section-wrapper">
            <div class="container">
                <p>Amazing Work Showcase</p>
            </div>
        </div>
    </div>
</div>
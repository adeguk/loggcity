
<div class="callout-box style2">
    <div class="container">
        <div class="callout-content">
            <div class="callout-text1">
                <p style="color:#fff"><span style="font-size:20px;font-weight:600; padding-top:15px">African-specific Economic outlook, Analysis and Forecasts</span><br>
                    <span class="dropcap style1">M</span>arketia - High quality financial news, insight, investors, report and other money matters from the heart of Africa to the world
                    <br><strong>featuring:</strong> financial outlook | Key facts about Economic Outlook | Policy Papers | datasets
                </p>                    
            </div>
            <div class="callout-action">
                <a class="btn style4" href="https://marketia.com">know more</a>
            </div>
        </div>
    </div>
</div>
<div class="social-auth-links text-center">
    <h4 style="padding-left:5%;color:#d58a20;"><em><b>Or Connect with!</b></em> </h4>
    <p class="or-social" style="margin-top: -10px;margin-bottom: 30px;"> your Social Network</p>

    <a href="{{ route('auth.provider', ['provider' => 'linkedin']) }}"
        class="button btn-block btn-social btn-linkedin" style="font-weight: normal;">
        <i class="fa fa-linkedin snIcon"></i> Continue with  <strong>LinkedIn</strong>
    </a>
    <a href="{{ route('auth.provider', ['provider' => 'twitter']) }}"
        class="button btn-block btn-social btn-twitter" style="font-weight: normal;">
        <i class="fa fa-twitter snIcon"></i> Continue with  <strong>Twitter</strong>
    </a>
    <a href="{{ route('auth.provider', ['provider' => 'google']) }}"
        class="button btn-block btn-social btn-google" style="font-weight: normal;">
        <i class="fa fa-google-plus snIcon"></i> Continue with <strong>Google<sup>+</sup></strong>
    </a>
</div>

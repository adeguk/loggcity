<div id="slideshow">
            <div class="revolution-slider">
                <ul>    <!-- SLIDE  -->
                    <!-- Slide1 -->
                    <li data-transition="zoomin" data-slotamount="7" data-masterspeed="1500">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('images/slider/innovation-at-your-finger-tips.jpg') }}" alt="Loggcity - Innovation at<br>Your Finger Tips">
                        <!-- LAYER NR. 17 -->
                        <div class="tp-caption very_big_white sfb" style="border-bottom:1px solid orange; padding-bottom:20px;"
                            data-x="left" data-y="center"
                            data-hoffset="0"  data-voffset="100"
                            data-speed="700" 
                            data-start="1700" 
                            data-easing="easeOutBack">
                            LOGGCITY:
                        </div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption very_big_white lfl" style="border-bottom:1px solid orange; padding-bottom:30px;"
                            data-x="left" data-y="center"
                            data-hoffset="0"  data-voffset="255"
                            data-speed="1350" 
                            data-start="1700" 
                            data-easing="easeOutBack">Innovation at<br>Your Finger Tips</div>
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption finewide_small_white sft"
                            data-x="left" data-y="center"
                            data-hoffset="0"  data-voffset="385"
                            data-speed="1700" 
                            data-start="1950" 
                            data-easing="easeOutBack">where NO simple means New Opportunity</div>
                    </li>
                    
                    <!-- Slide2 -->
                    <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1500">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('images/slider/happy-clients.jpg') }}" alt="Loggcity - Your Expert Business Consultant">
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption very_big_white sfb" style="border-bottom:1px solid #555; padding-bottom:20px;"
                            data-x="left" data-y="center"
                            data-hoffset="100"  data-voffset="100"
                            data-speed="700" 
                            data-start="1700" 
                            data-easing="easeOutBack">
                            LOGGCITY:
                        </div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption very_big_white lfl" style="border-bottom:1px solid #555; padding-bottom:20px;"
                            data-x="left" data-y="center"
                            data-hoffset="100"  data-voffset="255"
                            data-speed="1350" 
                            data-start="1700" 
                            data-easing="easeOutBack">Building Businesses<br> for the Next Generation</div>
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption finewide_small_white sft"
                            data-x="left" data-y="center"
                            data-hoffset="100"  data-voffset="385"
                            data-speed="1700" 
                            data-start="1950" 
                            data-easing="easeOutBack">Align you values, mission, work philosophy and business goal</div>
                    </li>
                    
                    <!-- Slide3 -->
                    <li data-transition="slidedown" data-slotamount="7" data-masterspeed="1500">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('images/slider/business-understanding.jpg') }}" alt="loggcity - most-influential business leader">
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption big_dark sfb" style="color:orange"
                            data-x="center" data-y="center"
                            data-voffset="100"
                            data-speed="700" 
                            data-start="1700" 
                            data-easing="easeOutBack">Strategic Business Leader
                        </div>
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption mediumwhitebg lfl text-center" style="border-bottom:1px solid #555;border-top:1px solid #555; padding-bottom:20px;padding-top:20px;"
                            data-x="center" data-y="center"
                            data-voffset="220"
                            data-speed="1350" 
                            data-start="1700" 
                            data-easing="easeOutBack">
                            helping SMEs, Government Agencies &amp; Institution create and execute<br> 
                            modern strategies that achieve exceptional growth; better processes, <br>
                            improved satisfaction and more leads, sales, and customers.</div>
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption finewide_small_white sft"
                            data-x="center" data-y="center"
                            data-voffset="310"
                            data-speed="1700" 
                            data-start="1950" 
                            data-easing="easeOutBack">Discover new opportunities in your business</div>
                    </li>
                </ul>
            </div>
        </div>
<div class="parallax box-lg parallax-image1" data-stellar-background-ratio="0.5">
    <div class="testimonials style2">
        <div class="container">
            <h2 class="testimonials-title">Buyers Feedback</h2>
            <div class="sky-carousel testimonial-carousel">
                <div class="sky-carousel-wrapper">
                    <ul class="sky-carousel-container">
                        <li>
                            <img src="http://placehold.it/92x92" alt="" />
                            <div class="sc-content">
                                <h2 class="testimonial-author">John Doe<small>CEO</small></h2>
                                <p>
                                    <em>Mircale</em> turned out to be one of the best website designs that I’ve seen in my life it is very <em>sophisticated, practical</em> and extremely <em>modern</em>.
                                </p>
                            </div>
                        </li>
                        <li>
                            <img src="http://placehold.it/92x92" alt="" />
                            <div class="sc-content">
                                <h2 class="testimonial-author">Jessica Martin<small>CEO</small></h2>
                                <p>
                                    <em>Mircale</em> turned out to be one of the best website designs that I’ve seen in my life it is very <em>sophisticated, practical</em> and extremely <em>modern</em>.
                                </p>
                            </div>
                        </li>
                        <li>
                            <img src="http://placehold.it/92x92" alt="" />
                            <div class="sc-content">
                                <h2 class="testimonial-author">John Smith<small>Manager</small></h2>
                                <p>
                                    <em>Mircale</em> turned out to be one of the best website designs that I’ve seen in my life it is very <em>sophisticated, practical</em> and extremely <em>modern</em>.
                                </p>
                            </div>
                        </li>
                        <li>
                            <img src="http://placehold.it/92x92" alt="" />
                            <div class="sc-content">
                                <h2 class="testimonial-author">John Doe<small>CEO</small></h2>
                                <p>
                                    <em>Mircale</em> turned out to be one of the best website designs that I’ve seen in my life it is very <em>sophisticated, practical</em> and extremely <em>modern</em>.
                                </p>
                            </div>
                        </li>
                        <li>
                            <img src="http://placehold.it/92x92" alt="" />
                            <div class="sc-content">
                                <h2 class="testimonial-author">Jessica Martin<small>CEO</small></h2>
                                <p>
                                    <em>Mircale</em> turned out to be one of the best website designs that I’ve seen in my life it is very <em>sophisticated, practical</em> and extremely <em>modern</em>.
                                </p>
                            </div>
                        </li>
                        <li>
                            <img src="http://placehold.it/92x92" alt="" />
                            <div class="sc-content">
                                <h2 class="testimonial-author">John Smith<small>Manager</small></h2>
                                <p>
                                    <em>Mircale</em> turned out to be one of the best website designs that I’ve seen in my life it is very <em>sophisticated, practical</em> and extremely <em>modern</em>.
                                </p>
                            </div>
                        </li>
                        <li>
                            <img src="http://placehold.it/92x92" alt="" />
                            <div class="sc-content">
                                <h2 class="testimonial-author">John Doe<small>CEO</small></h2>
                                <p>
                                    <em>Mircale</em> turned out to be one of the best website designs that I’ve seen in my life it is very <em>sophisticated, practical</em> and extremely <em>modern</em>.
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
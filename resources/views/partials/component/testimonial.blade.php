<div class="cd-testimonials-all" style="top: 60px;">
    <div class="cd-testimonials-all-wrapper">
        <ul>
            <li class="cd-testimonials-item">
                <p>Awesome to work with. Incredibly organized, easy to communicate with, responsive with next iterations and beautiful work</p>
                <div class="cd-author">
                    <img src="{{ asset('images/testimonial/avatar-1.jpg') }}" alt="Author image">
                    <ul class="cd-author-info">
    					<li>James Waltson</li>
    					<li>Founder, TalkCreative</li>
                    </ul>
                </div> <!-- cd-author -->
            </li>

            <li class="cd-testimonials-item">
                <p>I found the whole team at Loggcity Academy to be both professional and efficient their work
                    – they are ever supportive and empowering, allowing my organisation to secure the Training
                     every step of the way.</p>
                <div class="cd-author">
                    <img src="{{ asset('images/testimonial/avatar-2.jpg') }}" alt="Author image">
                    <ul class="cd-author-info">
    					<li>Taye David</li>
    					<li>Brighter Future</li>
                    </ul>
                </div> <!-- cd-author -->
            </li>

            <li class="cd-testimonials-item">
                <p>Loggcity's team gives our organization great support. They are knowledgeable, responsive and
                    thorough; and all this at an excellent value.</p>

                <div class="cd-author">
                    <img src="{{ asset('images/testimonial/avatar-3.jpg') }}" alt="Author image">
                    <ul class="cd-author-info">
    					<li>Stella Pete</li>
    					<li>Greenworks</li>
                    </ul>
                </div> <!-- cd-author -->
            </li>

            <li class="cd-testimonials-item">
                <p>Loggcity provides first class web design services with the highest professionalism.
                    Their staff is very passionate and they handle all my buisness needs.</p>

                <div class="cd-author">
                    <img src="{{ asset('images/testimonial/avatar-4.jpg') }}" alt="Author image">
                    <ul class="cd-author-info">
                        <li>Petria Skyline</li>
                        <li>Co-founder, Dmetro</li>
                    </ul>
                </div> <!-- cd-author -->
            </li>

            <li class="cd-testimonials-item">
                <p>Loggcity ltd is an excellent creative agency who are not only flexible, efficient and professional
                    but a group of people who really care about delivering results for you. </p>
                <div class="cd-author">
                    <img src="{{ asset('images/testimonial/folusho_olotua.jpg') }}" alt="folusho olotua">
                    <ul class="cd-author-info">
                        <li>Prof. Olotuah</li>
                        <li>srannals.com.ng, AAUA</li>
                    </ul>
                </div> <!-- cd-author -->
            </li>

        </ul>
    </div>	<!-- cd-testimonials-all-wrapper -->

    <a href="#all-testimonials" class="close-btn">Close</a>
</div>

<div class="section">
    <div class="container">
        <div class="heading-box">
            <h2 class="box-title">Creative People Behind the Curtain</h2>
            <p class="desc-lg">Meet the creative heads behind all the Miracle :)</p>
        </div>
        <div class="row">
            <div class="col-sms-6 col-sm-6 col-md-3">
                <div class="team-member style-default">
                    <div class="image-container">
                        <img src="http://placehold.it/270x305" alt="">
                    </div>
                    <div class="team-member-author">
                        <h4 class="team-member-name">Aleesha Brown</h4>
                        <span class="team-member-job">Marketing</span>
                    </div>
                    <div class="team-member-social">
                        <div class="social-icons style1 size-sm">
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sms-6 col-sm-6 col-md-3">
                <div class="team-member style-default">
                    <div class="image-container">
                        <img src="http://placehold.it/270x305" alt="">
                    </div>
                    <div class="team-member-author">
                        <h4 class="team-member-name">Mike Harward</h4>
                        <span class="team-member-job">administration</span>
                    </div>
                    <div class="team-member-social">
                        <div class="social-icons style1 size-sm">
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sms-6 col-sm-6 col-md-3">
                <div class="team-member style-default">
                    <div class="image-container">
                        <img src="http://placehold.it/270x305" alt="">
                    </div>
                    <div class="team-member-author">
                        <h4 class="team-member-name">Adelia Lorene</h4>
                        <span class="team-member-job">Development</span>
                    </div>
                    <div class="team-member-social">
                        <div class="social-icons style1 size-sm">
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sms-6 col-sm-6 col-md-3">
                <div class="team-member style-default">
                    <div class="image-container">
                        <img src="http://placehold.it/270x305" alt="">
                    </div>
                    <div class="team-member-author">
                        <h4 class="team-member-name">Chris Dand</h4>
                        <span class="team-member-job">design</span>
                    </div>
                    <div class="team-member-social">
                        <div class="social-icons style1 size-sm">
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="social-icon" data-toggle="tooltip" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
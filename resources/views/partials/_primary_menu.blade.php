<div class="container">
        <div class="header-inner">
            <div class="branding">
                <h1 class="logo">
                    <a href="{{url('/')}}"><img src="{{ asset('images/logo@2x.png') }}" alt="" width="25" height="26">Loggcity</a>
                </h1>
            </div>
            <nav id="nav" class="style-colored">
                <ul class="header-top-nav">
                    <li class="mini-search">
                        <a href="#"><i class="fa fa-search has-circle"></i></a>
                        <div class="main-nav-search-form">
                            <form method="get" role="search">
                                <div class="search-box">
                                    <input type="text" id="s" name="s" value="">
                                    <button type="submit"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>
                    </li>
                    <li class="visible-mobile">
                        <a href="#mobile-nav-wrapper" data-toggle="collapse"><i class="fa fa-bars has-circle"></i></a>
                    </li>
                </ul>
                <ul id="main-nav" class="hidden-mobile">
                    <li>
                        <a href="{{url('/')}}"><i class="fa fa-home"></i></a>
                    </li>
                    <li class="menu-item-has-children mega-menu-item mega-column-4">
                        <a href="#">Services</a>
                        <ul class="sub-nav">
                            <li class="menu-item-has-children">
                                <a href="#">Support Solutions</a>
                                <ul class="sub-nav">
                                    <li><a href="javascript:void(0)"><i class="fa fa-wrench"></i><span>IT Support</span></a></li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-spinner"></i><span>Business Continuity</span></a></li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-file-text-o"></i><span>GDPR Consultation</span></a></li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-truck"></i><span>IT Relocation</span></a></li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-desktop"></i><span>Application Infrastruture</span></a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Business Solutions</a>
                                <ul class="sub-nav">
                                    <li><a href="javascript:void(0)"><i class="fa fa-users"></i><span>IT Consultancy</span></a></li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-exchange"></i><span>Recruitment &amp; Outsourcing</span></a></li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-tasks"></i><span>Strategic Management</span></a></li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-tags"></i><span>Product Supply</span></a></li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-th-list"></i><span>Project Management</span></a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Divisonal Services</a>
                                <ul class="sub-nav">
                                    <li><a href="javascript:void(0)"><i class="fa fa-th"></i><span>Construction Management</span></a></li>
                                    <li><a href="https://digital.loggcity.uk"><i class="fa fa-code"></i><span>Digital Services</span></a></li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-leaf"></i><span>e-Agriculture Implementation</span></a></li>
                                    <li><a href="javascript:void(0)"><i class="fa fa-search"></i><span>Research &amp; Development</span></a></li>
                                    <li><a href="https://iknow.loggcity.uk"><i class="fa fa-book"></i><span>Training &amp; Development</span></a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <img src="{{ asset('images/slider/service-system.jpg') }}" alt="Loggcity - Innovation at<br>Your Finger Tips" style="margin-bottom:10px">
                               <p style="color:#fff">We handle exceptional and complex requirements with confidence and passion -
                               by team of business leaders working across multiple divisions.</p>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children mega-menu-item mega-column-4">
                        <a href="#">Industries</a>
                        <ul class="sub-nav">
                            <li class="menu-item-has-children">
                                <img src="{{ asset('images/slider/team-idea-innovation.jpg') }}" alt="Loggcity - Innovation at<br>Your Finger Tips" style="margin-bottom:10px">
                            </li>
                            <li class="menu-item-has-children">                                
                                <h5 style="color:#fcfcfc;line-height:1.55">Loggcity has a long and focused expertise that improves your business performance.
                                    Our team has extensive corporate and on-the-ground experience designing and realizing sustainable strategies, partnership 
                                    and programs in these industries.</h5>
                            </li>
                            <li class="menu-item-has-children">
                                <ul class="sub-nav">
                                    <li><a href="shortcode-post-sliders.html"><i class="fa fa-shopping-cart"></i><span>Consumer &amp; Retail</span></a></li>
                                    <li><a href="shortcode-typography.html"><i class="fa fa-th"></i><span>Construction &amp; Real Estate</span></a></li>
                                    <li><a href="shortcode-progress-bars.html"><i class="fa fa-book"></i><span>Education</span></a></li>
                                    <li><a href="shortcode-pricing-tables.html"><i class="fa fa-credit-card"></i><span>FinTech &amp; Financial</span></a></li>
                                    <li><a href="shortcode-style-changer.html"><i class="fa fa-medkit"></i><span>Healthcare</span></a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <ul class="sub-nav">
                                    <li><a href="shortcode-tabs.html"><i class="fa fa-truck"></i><span>Logistics</span></a></li>
                                    <li><a href="shortcode-team.html"><i class="fa fa-caret-square-o-right"></i><span>Media &amp; Entertainment</span></a></li>
                                    <li><a href="shortcode-style-changer.html"><i class="fa fa-anchor"></i><span>Public Sector</span></a></li>
                                    <li><a href="shortcode-testimonials.html"><i class="fa fa-user"></i><span>Recruitment</span></a></li>
                                    <li><a href="shortcode-tabs.html"><i class="fa fa-desktop"></i><span>Technology</span></a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="#">Community</a>
                        <ul class="sub-nav">
                            <li><a href="#">Emerging Techs</a></li>
                            <li class="menu-item-has-children">
                                <a href="#">Events</a>
                                <ul class="sub-nav">
                                    <li><a href="javascript:void(0)">Code Gladiators</a></li>
                                    <li><a href="javascript:void(0)">Kodeous</a></li>
                                    <li><a href="javascript:void(0)">All Past Events</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Meet-Ups</a>
                                <ul class="sub-nav">
                                    <li><a href="javascript:void(0)">Coding 4 Kids</a></li>
                                    <li><a href="javascript:void(0)">LaraStudent</a></li>
                                    <li><a href="javascript:void(0)">PHP Gods</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Patners</a>
                                <ul class="sub-nav">
                                    <li><a href="javascript:void(0)">Our Partner</a></li>
                                    <li><a href="javascript:void(0)">Become a Partner</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Our Clients</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="#">Company</a>
                        <ul class="sub-nav">
                            <li><a href="{{route('pages.show', 'in-brief')}}">In Brief</a></li>
                            <li class="menu-item-has-children">
                                <a href="#">Our Division</a>
                                <ul class="sub-nav">
                                    <li><a href="javascript:void(0)">Capide</a></li>
                                    <li><a href="https://digital.loggcity.uk">Loggcity Digital</a></li>
                                    <li><a href="https://iknow.loggcity.uk">iKnow: Loggcity Academy</a></li>
                                    <li><a href="javascript:void(0)">Lumiverse</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Insight</a>
                                <ul class="sub-nav">
                                    <li><a href="{{route('pages.show', 'fact-and-figure')}}">The Figure</a></li>
                                    <li><a href="{{route('pages.show', 'in-brief')}}">Current Projects</a></li>
                                    <li><a href="{{route('pages.show', 'the-future')}}">The Future</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Career</a></li>
                            <li><a href="{{url('/getting-in-touch')}}">Getting in Touch</a></li>                            
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="#">Resources</a>
                        <ul class="sub-nav">
                            <li><a href="#">Downloads</a></li>
                            <li><a href="http://gateway.loggcity.uk/knowledge-base">Knowledge-base</a></li>
                            <li><a href="{{url('/blog')}}">Media Relation</a></li>
                            <li class="menu-item-has-children">
                                <a href="#">Publications</a>
                                <ul class="sub-nav">
                                    <li><a href="woocommerce-category-grid.html">WhitePaper</a></li>
                                </ul>
                            </li>
                            <!--li class="menu-item-has-children">
                                <a href="#">Product Pages</a>
                                <ul class="sub-nav">
                                    <li><a href="woocommerce-product-detailed.html">Product Detailed</a></li>
                                    <li><a href="woocommerce-product-sidebar.html">Product Sidebar</a></li>
                                </ul>
                            </li>
                            <li><a href="woocommerce-product-quickview.html">Quick View Popup</a></li>
                            <li class="menu-item-has-children">
                                <a href="#">Cart Pages</a>
                                <ul class="sub-nav">
                                    <li><a href="woocommerce-shopping-cart.html">Shopping Cart</a></li>
                                    <li><a href="woocommerce-checkout.html">Checkout</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#">Page Layouts</a>
                                <ul class="sub-nav">
                                    <li><a href="woocommerce-layout-fullwidth.html">Fullwidth</a></li>
                                    <li><a href="woocommerce-layout-sidebar.html">Sidebar</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="woocommerce-dashboard.html">Dashboard</a>
                            </li-->
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light  sticky-top megamenu">

        <!-- Brand and toggle get grouped for better mobile display -->
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav navbar-nav mr-auto">
                <li class="active"><a href="#" class="nav-link">Link <span class="sr-only">(current)</span></a></li>
                <li class="nav-item"><a href="#" class="nav-link">Link</a></li>
                <li class="nav-item dropdown megamenu-fw">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Mega Menu <span class="caret"></span></a>
                    <ul class="dropdown-menu megamenu-content" role="menu">
                        <li>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <h3 class="title mt-3">Featured Products</h3>
                                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <a href="#"><img class="d-block w-100" src="http://placehold.it/350x200/3498db/f5f5f5/&amp;text=Featured+Product" alt="First slide"></a>
                                                <h6><small>Feature product 1</small></h6>
                                                <button type="button" class="btn btn-outline-secondary btn-sm">Add to List</button>
                                            </div>
                                            <div class="carousel-item">
                                                <a href="#"><img class="d-block w-100" src="http://placehold.it/350x200/ef5e55/f5f5f5/&amp;text=Featured+Product" alt="Second slide"></a>
                                                <h6><small>Feature product 2</small></h6>
                                                <button type="button" class="btn btn-outline-secondary btn-sm">Add to List</button>
                                            </div>
                                            <div class="carousel-item">
                                                <a href="#"><img class="d-block w-100" src="http://placehold.it/350x200/2ecc71/f5f5f5/&amp;text=Featured+Product" alt="Third slide"></a>
                                                <h6><small>Feature product 3</small></h6>
                                                <button type="button" class="btn btn-outline-secondary btn-sm">Add to List</button>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- end col-4 -->
                                <div class="col-xs-12 col-sm-4">
                                    <h3 class="title mt-3">Latest Tutorials</h3>
                                    <ul class="list-unstyled">
                                        <li class="media mb-3">
                                            <img class="mr-3" src="http://placehold.it/60x35/3498db/f5f5f5/&amp;text=Featured+Product" alt="Generic placeholder image">
                                            <div class="media-body">
                                                <a href="#">How To Master Fireworks’ CSS Properties Panel And CSS Professionalzr</a>
                                            </div>
                                        </li>
                                        <li class="media mb-3">
                                            <img class="mr-3" src="http://placehold.it/60x35/ef5e55/f5f5f5/&amp;text=Featured+Product" alt="Generic placeholder image">
                                            <div class="media-body">
                                                <a href="#">Creating A “Save For Later” Chrome Extension With Modern Web Tools</a>
                                            </div>
                                        </li>
                                        <li class="media mb-3">
                                            <img class="mr-3" src="http://placehold.it/60x35/2ecc71/f5f5f5/&amp;text=Featured+Product" alt="Generic placeholder image">
                                            <div class="media-body">
                                                <a href="#">Making A Complete Polyfill For The HTML5 Details Element</a>
                                            </div>
                                        </li>
                                        <li class="media mb-3">
                                            <img class="mr-3" src="http://placehold.it/60x35/3498db/f5f5f5/&amp;text=Featured+Product" alt="Generic placeholder image">
                                            <div class="media-body">
                                                <a href="#">Scaling Down The BEM Methodology For Small Projects</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div><!-- end col-4 -->
                                <div class="col-xs-12 col-sm-4">
                                    <h3 class="title mt-3">Google Map</h3>
                                    <iframe width="100%" height="240" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://my.ctrlq.org/maps/#roadmap|10|41.001769314909254|-74.88742636132815"></iframe>
                                </div><!-- end col-4 -->
                            </div><!-- end row -->
                            <hr class="mb-4 mt-5">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h3 class="title mt-3">About Us</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                </div><!-- end col-6 -->
                                <div class="col-sm-3">
                                    <h3 class="title mt-3">Follow Us</h3>
                                    <p></p>
                                </div><!-- end col-3 -->
                                <div class="col-sm-3">
                                    <h3 class="title mt-3">Subscribe</h3>
                                    <form class="navbar-form nopadding" method="post" action="">
                                        <input type="text" class="form-control" placeholder="Enter email" name="email">
                                        <input type="submit" value="Go" class="btn btn-primary">
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Gallery
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" role="menu">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Separated link</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">One more separated link</a>
                    </div>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0" role="search">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" class="nav-link">Link</a></li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</div>
@extends('layouts.page')

@section('content')
    <section class="mbr-section" style="background:url({{asset('images/workinglate_1920.jpg')}}) no-repeat 0% 10%;height: 270px;">
        <div class="container"></div>
    </section>

<section class="about-us-area mbr-section mbr-section-md-padding white">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-xs-12 wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                <h3 class="page-header fw3" style="margin-top: 0;">Our Focus</h3>
                <p class="para">At Loggcity, we handle complexities,
                    expectional specifications and challenges with great care, passion and confidence. These services
                    are delivered by teams working across multiple divisions.
                </p>
                <p class="para">Our people are equipped with dynamic proficiency and our expertise lies in providing
                    outstanding services to our clients.</p>
            </div>
            <div class="col-md-8 col-xs-12 wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                <div role="tablist" id="accordion" class="panel-group my-accordion about-us">
                    <div class="panel panel-default">
                        <div id="IT-Management-Solution" role="tab" class="panel-heading">
                            <h4 class="panel-title">
                                <a aria-controls="collapseIT-Management-Solution" aria-expanded="false"
                                   href="#collapseIT-Management-Solution" data-parent="#accordion" data-toggle="collapse">
                                    IT MANAGEMENT SERVICES
                                </a>
                            </h4>
                        </div>
                        <div aria-labelledby="IT-Management-Solution" role="tabpanel" class="panel-collapse collapse" id="collapseIT-Management-Solution">
                            <div class="panel-body">
                                <p class="para">The way business operates in the new era is shifting due to information
                                    technology, ensuring that information is shared, managed and acted upon. At
                                    Loggcity, we recognise these wide range of technologies and solutions in the
                                    marketplace. We guide our customers business through different stages of introducing
                                    or updating ICT within their organisation by reviews of their business and operational
                                    requirements. This includes Business Analysis, IT Project Management, IT process mapping, ICT road-mapping and product supply.
                                </p>
                                <p>
                                    As such, we ensure that we maintain unsurpassed access to the best consultants and
                                    technical specialists working in IT today. Our service offerings are a range of
                                    consultancy skills that can be used either individually, or in teams to deliver
                                    complex solutions to our clients need.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div id="Recruitment-and-outsourcing" role="tab" class="panel-heading">
                            <h4 class="panel-title">
                                <a aria-controls="collapseTraining-and-development" aria-expanded="true"
                                   href="#collapseRecruitment-and-outsourcing" data-parent="#accordion"
                                   data-toggle="collapse" class="collapsed">
                                    RECRUITMENT AND OUTSOURCING
                                </a>
                            </h4>
                        </div>
                        <div aria-labelledby="Recruitment-and-outsourcing" role="tabpanel"
                             class="panel-collapse collapse" id="collapseRecruitment-and-outsourcing">
                            <div class="panel-body">
                                <p class="para">
                                    Loggcity is able to serve all manner of outsourcing needs, from supply of the
                                    pool of our workforce on short-, medium- or long-term contracts to managing
                                    projects on clients&acute; behalf – either onshore or offshore. We have specialist
                                    teams that can come in on a consultative basis or act as project managers within
                                    the client&acute; organisation. A good example of our flexible benefits to customers.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div id="Training-and-development" role="tab" class="panel-heading">
                            <h4 class="panel-title">
                                <a aria-controls="collapseTraining-and-development" aria-expanded="true"
                                   href="#collapseTraining-and-development" data-parent="#accordion"
                                   data-toggle="collapse" class="collapsed">
                                    TRAINING &amp; DEVELOPMENT
                                </a>
                            </h4>
                        </div>
                        <div aria-labelledby="Training-and-development" role="tabpanel"
                             class="panel-collapse collapse" id="collapseTraining-and-development">
                            <div class="panel-body">
                                <p>
                                    Loggcity Academy provides high quality and enjoyable training experiences.
                                    We provide our students a first-class and improved education that enables them, in all aspects of their lives,
                                    to make the progress and achieve the standards that reflect their full ability in a language best known to them.
                                </p>
                                <a href="/courses" class="btn btn-raised btn-success btn-lg">Learn More</a>
                                <a href="/courses" class="btn btn-raised btn-primary btn-lg">Browse Our Courses</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div id="Web-Mobile-Dev" role="tab" class="panel-heading">
                            <h4 class="panel-title">
                                <a aria-controls="collapseWeb-Mobile-Dev" aria-expanded="false" href="#collapseWeb-Mobile-Dev" data-parent="#accordion" data-toggle="collapse" class="collapsed">
                                    Web &amp; Mobile Dev.
                                </a>
                            </h4>
                        </div>
                        <div aria-labelledby="Web-Mobile-Dev" role="tabpanel" class="panel-collapse collapse" id="collapseWeb-Mobile-Dev">
                            <div class="panel-body">
                                <p>
                                    We convert all ideas to the reality with stunning User Experience and Design for businesses
                                    that are looking to increase their sales and growth. We’re obsessed with creating quality responsive websites.
                                    We’re here to help with logo design, graphic design, brand development, advertising, digital marketing strategy and web design.
                                </p>
                                <p>Our pool of professionals have extensive experience across a broad spectrum
                                    of latest technologies for great Frontend design and secure backend.
                                </p>
                                <a href="http://digital.loggcity.uk" class="btn-raised btn-warning btn" alt="Loggcity Digital - Web and Mobile Appz">VISIT LOGGCITY DIGITAL</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div id="IT-Support" role="tab" class="panel-heading">
                            <h4 class="panel-title">
                                <a aria-controls="collapseIT-Support" aria-expanded="false" href="#collapseIT-Support"
                                   data-parent="#accordion" data-toggle="collapse" class="collapsed">
                                    IT SUPPORTS
                                </a>
                            </h4>
                        </div>
                        <div aria-labelledby="IT-Support" role="tabpanel" class="panel-collapse collapse" id="collapseIT-Support">
                            <div class="panel-body">
                                <p class="para">
                                    Our Support Centre is designed to give you complete transparency.
                                    No hidden costs, no additional extras, just a service you can rely on and most
                                    importantly trust. These services encompass issues around:
                                </p>
                                <ul>
                                    <li>Data backup and maintenance</li>
                                    <li>OnSite and Remote support and maintenance</li>
                                    <li>Prioritizing calls and attending customer sites according to SLA</li>
                                    <li>Hardware &amp; Software repair and configuration</li>
                                    <li>Network support and active directory</li>
                                    <li>Analyzing and troubleshooting issues</li>
                                </ul>
                                <a class="btn btn-raised btn-warning" href="it-support.php">LEARN MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div id="Research-and-Development" role="tab" class="panel-heading">
                            <h4 class="panel-title">
                                <a aria-controls="collapseResearch-and-Development" aria-expanded="false"
                                   href="#collapseResearch-and-Development" data-parent="#accordion" data-toggle="collapse" class="collapsed">
                                    Research and Development
                                </a>
                            </h4>
                        </div>
                        <div aria-labelledby="Research-and-Development" role="tabpanel"
                             class="panel-collapse collapse" id="collapseResearch-and-Development">
                            <div class="panel-body">
                                <p class="para">
                                    Our research and development unit is one of the strategic arms of our firm, playing
                                    a critical role in our investment process by providing comprehensive research on the
                                    global economy, technological advancement and changes across industries.
                                    Coupled with practical skill, our R&amp;D team aids our consultants provide
                                    financial and strategic advice to our clients to achieve their business goals.
                                </p>
                                <a class="btn btn-raised btn-warning" href="javascript:void(0)">LEARN MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section mbr-section-small red darken-4 white-text">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3 class="page-header fw3">... our promise to you ...</h3>
                <p>We provide <big>support, training, documentation and other on-going</big> after sakes service as key elements of
                    our long-term relationship with our customer when necessary. We discuss these options at an appropriate
                    stage in the contract negotiations, largely because each client's need often vary considerably and
                    we consider each client as unique to us.
                </p>
                </p>
            </div>
        </div>
</section>

@endsection
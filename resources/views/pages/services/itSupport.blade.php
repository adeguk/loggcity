@extends('layouts.page')

@section('content')
    <section class="mbr-section mbr-section-md-padding white">
        <div class="container" id="aboutUs">
            <div class="row">
                <div class="col-xs-12 col-sm-7">
                    <h3 class="page-header fw3">9 Reasons to choose us for your support</h3>
                    <ul class="features-list">
                        <li><i class="fa fa-angle-right"></i>24/7 availability</li>
                        <li><i class="fa fa-angle-right"></i>Competitively and low cost</li>
                        <li><i class="fa fa-angle-right"></i>Personal Account Manager</li>
                        <li><i class="fa fa-angle-right"></i>Dedicated helpdesk so that you can immediately talk to a senior engineer</li>
                        <li><i class="fa fa-angle-right"></i>Continually monitoring of your systems to spot any potential issues in advance</li>
                        <li><i class="fa fa-angle-right"></i>Preventative maintenance visits to ensure close working relationship with you and reduce cost</li>
                        <li><i class="fa fa-angle-right"></i>Excellence customer service that make difference</li>
                        <li><i class="fa fa-angle-right"></i>Effective and efficient response time</li>
                        <li><i class="fa fa-angle-right"></i>Provision comprehensive knowledge-base where necessary</li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-5">
                    <img style="width:100%" src="{{asset('images/services_banner_md.jpg')}}" alt="why you will work with us">
                    <p class="para">We provides cost-effective IT support services to hundreds of businesses nationwide.
                        Loggcity typically supports clients from various industrial sector including government, education, banking;
                        and specialise in IT support,
                        web maintenance, computer services and applications tailored for SMEs.</p>
                    <p class="para">
                        We look after our clients' systems and take the hassle out of their IT, so they can focus on
                        running their businesses.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="mbr-section teal mbr-section-small darken-4" id="more-details">
        <div class="container">
            <div class="row"><!-- Nav tabs -->
                <ul class="nav nav-tabs" style="margin-bottom:15px;" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#ad-hoc" role="tab">Ad hoc support</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#ras" role="tab">Remote Access Support</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#oss" role="tab">On-Site Support</a>
                    </li>
                </ul>
                <div class="tab-content white-text"><!-- Tab panes -->
                    <div class="tab-pane active" id="ad-hoc" role="tabpanel">
                        <p>Loggcity can provide specialist IT assistance when you most need it. Our ad hoc IT
                            support package gives you the flexibility to:</p>
                        <ul class="features-list">
                            <li class="white-text" style="padding-left:0">Pay as you go to resolve IT helpdesk issues</li>
                            <li class="white-text" style="padding-left:0">Pre-purchase our time at discounted labour rates</li>
                            <li class="white-text" style="padding-left:0">Get IT support and resources when you most need them</li>
                            <li class="white-text" style="padding-left:0">Provide the right resources when there is a lack of specific skills</li>
                            <li class="white-text" style="padding-left:0">Benefit from IT support to cover for holiday or staff sickness</li>
                        </ul>
                    </div>
                    <div class="tab-pane" id="ras" role="tabpanel">
                        <p>Our support clients benefit from unlimited telephone and remote access support to
                            ensure that any issues are dealt with quickly and cost-effectively. All incidents are
                            logged into our comprehensive helpdesk system and dealt with by one of our Certified Engineers.</p>
                        <p>Our helpdesk system allows us to manage incidents, review any historical problems and
                            quickly resolve the problem to minimise any effects on your business. A secure connection
                            is set up and our engineers remotely connect to your machine to resolve any
                            issues with minimum inconvenience and eliminate security risk</p>
                    </div>
                    <div class="tab-pane active" id="oss" role="tabpanel">
                        <p>We pride ourselves on providing a prompt and proactive service with response times to
                            fit in with your needs. Critical applications or machines will be attended todo
                            based on individual SLA. More importantly, our proactive approach to IT support means
                            we can identify and address potential problems in advance.</p>
                        <p>We carry out regular preventative maintenance visits for the majority of our clients,
                            which significantly reduce the number of support calls you’ll need to make.
                            This provide a great opportunity for you to discuss your systems and
                            any potential improvements you might require with your preferred engineer.
                            We provide internal IT department, preventing you from issues and unnecessary cost.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="mbr-section mbr-section-md-padding white">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="fw4">Infrastructure Management</h4>
                        </div>
                        <div class="card-image">
                            <img class="card-img-top" src="{{asset('images/application.jpg')}}" alt="Infrastructure Management">
                        </div>
                        <div class="card-body">
                            <div class="card-text">
                                <p>We help reduce the time, cost &amp; stress of wide-scale business application management.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="fw4">Managed Support</h4>
                        </div>
                        <div class="card-image">
                            <img class="card-img-top" src="{{asset('images/managed-services.jpg')}}" alt="Managed Services">
                        </div>
                        <div class="card-body">
                            <div class="card-text">
                                <p>Affordable support plans to avoid IT disaster, frustrations or downtime.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="fw4">Disaster Recovery</h4>
                        </div>
                        <div class="card-image">
                            <img class="card-img-top" src="{{asset('images/abstract-vector-and-hd-digital-art-wide_md.jpg')}}" alt="Disaster Recovery">
                        </div>
                        <div class="card-body">
                            <div class="card-text">
                                <p>We have the knowledge to create the correct plan tailored to your individual needs and budget.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="fw4">Data Management</h4>
                        </div>
                        <div class="card-image">
                            <img class="card-img-top" src="{{asset('images/big-data_md.jpg')}}" alt="Data Management">
                        </div>
                        <div class="card-body">
                            <div class="card-text">
                                <p>Regular data backups dramatically reduce the risk of having to recover from data loss.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
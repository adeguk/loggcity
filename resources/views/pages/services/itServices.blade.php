@extends('layouts.page')

@push('css')
<link href="{{ asset('vendor/diamond/diamond.css') }}" rel="stylesheet">
@endpush

@section('content')
    <section class="about-us-area mbr-section mbr-section-small deep-orange darken-4 hidden-xs">
        <div class="container">
            <div class="row">
                <div class="diamond-grid-container wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                    <ul class="diamond-grid">
                        <li>
                            <a href="#" target="_blank" class="diamond wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                                <div id="diamond-1">
                                    <div class="text">
                                        IT Management and Solution
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank" class="diamond wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                                <div id="diamond-2">
                                    <div class="text">
                                        IT Product Supply
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank" class="diamond wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                                <div id="diamond-3">
                                    <div class="text">
                                        Data &amp; Information Management
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank" class="diamond wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                                <div id="diamond-4">
                                    <div class="text">
                                        Research &amp; Development
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank" class="diamond wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                                <div id="diamond-5">
                                    <div class="text">
                                        Consulting &amp; Development
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank" class="diamond wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                                <div id="diamond-6">
                                    <div class="text">
                                        Strategic Management
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank" class="diamond wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                                <div id="diamond-7">
                                    <div class="text">
                                        Support &amp; Maintenance
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank" class="diamond wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                                <div id="diamond-8">
                                    <div class="text">
                                        Training <br>and Staff Outsourcing
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank" class="diamond wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                                <div id="diamond-9">
                                    <div class="text">
                                        Web &amp; Mobile Solutions
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" target="_blank" class="diamond wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                                <div id="diamond-10">
                                    <div class="text">
                                        Project Consulting &amp; Management
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="about-us-area mbr-section mbr-section-md-padding white">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-image"><img class="card-img-top" src="{{asset('images/executiveTraining.jpg')}}" alt="IT Consultancy"></div>
                        <div class="card-body">
                            <h4 class="card-title">
                                <i class="fa fa-cloud"></i>
                                IT Consultancy
                            </h4>
                            <div class="card-text">
                                <p>The way business operates is shifting due to information technology,
                                    ensuring that information is shared, managed and acted upon. </p>
                                <p>We guide customers business through different stages of introducing or updating ICT
                                    within their organisation by review of their business and operational requirements.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-image"><img class="card-img-top" src="{{asset('images/application_full.jpg')}}" alt="Application Development"></div>
                        <div class="card-body">
                            <h4 class="card-title">
                                <i class="fa fa-cloud"></i>
                                Application Development
                            </h4>
                            <div class="card-text">
                                <p>With our pool of professionals across London, India and Nigeria, we provide
                                    creative and innovative Web Design, E-Marketing and SEO, E-Commerce Solutions, Mobile App
                                    as well as seamless integration with various third party solutions. </p>
                            </div>
                            <a href="http://digital.loggcity.uk" class="btn btn-raised btn-info">Learn More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-image"><img class="card-img-top" src="{{asset('images/playingchess.jpg')}}" alt="Strategic Management"></div>
                        <div class="card-body">
                            <h4 class="card-title">
                                <i class="fa fa-cloud"></i>
                                Strategic Management
                            </h4>
                            <div class="card-text">
                                <p>In a world where the swiftness of information is uncontrollable and technological
                                    integration in business is always developing, we will guide our client via
                                    different stages of introducing and/or updating
                                    ICT within their organisation by review of their existing technology, business and operational
                                    requirements. This also include on-going advice and support, disaster recovery
                                    procedures and security assessment. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-image"><img class="card-img-top" src="{{asset('images/it_consultancy.jpg')}}" alt="IT Product and staff outsourcing"></div>
                        <div class="card-body">
                            <h4 class="card-title">IT Outsourcing</h4>
                            <div class="card-text">
                                <p>We are able to serve all forms of outsourcing needs, from supply of our workforce
                                    on short and/or long-term contract to managing projects on clients'
                                    behalf – either onshore or offshore. We have specialist teams that can come in on a
                                    consultative basis or act as project managers..
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-image"><img class="card-img-top" src="{{asset('images/UK-Logistics-Forum1.jpg')}}" alt="office relocation"></div>
                        <div class="card-body">
                            <h4 class="card-title">Office Relocations</h4>
                            <div class="card-text">
                                <p>Moving offices is a daunting project and knowing that your computer systems will be
                                    safely relocated, and installed in a working condition in your new premises,
                                    takes much of the pressure off.</p>
                            </div>
                            <a href="javascript:void(0)" class="btn btn-raised btn-info">Learn More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Innovative Research &amp; Development</h4>
                            <div class="card-text">
                                <p>Loggcity is re-investing in R&amp;D to promote business success and knowledge management.
                                    Our research and development unit is one of the strategic arms of our firm,
                                    playing a critical role in our investment process by providing comprehensive
                                    research on the global economy, technological advancement, and changes across
                                    industries. Coupled with practical skill, our research and development service helps
                                    provide financial and strategic advice to our clients to achieve their business goals.
                                </p><p>Our R&amp;D team includes more than 50 leading researchers across the global from
                                    academic researchers, applications, technical and many more.</p>
                                <p>At Loggcity, our goal is to influence R&amp;D capabilities across the nation to create
                                    unique value through new, improved products and services and innovative ways to
                                    cost reduction. We aim to give our clients access to variety of resources much
                                    possible to include databases, online research, and libraries. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
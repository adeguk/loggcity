@extends('layouts.page')

@section('content')
    @isset($page->image)
        <section class="mbr-section" style="background:url({{asset('storage/'.$page->image)}}) no-repeat 0% 10%;height: 270px;">
            <div class="container"></div>
        </section>
    @endisset
    @if(isset($page->body))
        {!!$page->body!!}
    @endif
@endsection
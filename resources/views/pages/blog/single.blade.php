@extends('layouts.page')
@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.carousel.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.transitions.css') }}" media="screen" />
    <!-- Magnific Popup core CSS file -->
    <link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}">
@endpush
@push('headerbottom')
    <div class="page-title-container style7">
        <div class="page-title">
            <div class="container">
                <h1 class="entry-title">{{$page->article_title}}</h1>
            </div>
        </div>
        <ul class="breadcrumbs">
            <li><a href="{{url('/')}}">Home</a></li>
            <li><a href="{{url('/blog')}}">Media Relations</a></li>
            <li>{{$page->category->name or 'Uncategories'}}</li>
            <li class="active">{{$page->title}}</li>
        </ul>
    </div>
@endpush

@section('content')
    <div class="container">
        <div id="main">
            <article class="post box-lg">
                <div class="post-date">
                    <span class="day">{{date("d", strtotime($page->created_at))}}</span>
                    <span class="month">{{date("M", strtotime($page->created_at))}}</span>
                </div>
                @isset($page->image)
                    <div class="post-image">
                        <div class="image-container" style="height: 400px; overflow: hidden;">
                            <a href="javascript:void(0)"><img src="{{asset('storage/'.$page->image)}}" alt="{{$page->title}}" title="{{$page->title}}" /></a>
                        </div>
                    </div>
                @endisset
                @empty($page->image)
                    <div class="post-image">
                        <div class="image-container">
                            <a href="javascript:void(0)"><img alt="" src="http://placehold.it/1103x408" alt="{{$page->title}}" title="{{$page->title}}" ></a>
                        </div>
                    </div>
                @endempty
                <div class="post-content">
                    <div class="post-action">
                        <a href="#" class="btn btn-sm"><i class="fa fa-heart"></i>480</a>
                        <a href="#" class="btn btn-sm"><i class="fa fa-share"></i>Share</a>
                    </div>
                    <h2 class="entry-title">{{$page->article_title}}</h2>
                    <div class="post-meta">
                        <span class="entry-author fn">by <a href="javascript:void(0)">{{$page->custom_author or $page->author->name}}</a></span>
                        <span class="post-category">in <a href="javascript:void(0)">{{$page->category->name or 'Uncategories'}}</a></span>
                        <span class="post-comment"><a href="#">1 Comment</a></span>
                    </div>
                    {!! $page->body !!}
                    <div class="tags">
                        <a href="#" class="tag">Masonry</a>
                        <a href="#" class="tag">Responsive</a>
                        <a href="#" class="tag">Retina</a>
                    </div>
                </div>
                <!--h3 class="font-normal">Related Posts</h3>
                <div class="related-posts clearfix box-sm same-height">
                    <div class="related-post col-sm-6 col-md-4">
                        <article class="post">
                            <div class="post-image">
                                <div class="img">
                                    <img src="http://placehold.it/150x150" alt="">
                                </div>
                            </div>
                            <div class="details">
                                <h5 class="post-title"><a href="#">Libero molestienulla lorem ipsum</a></h5>
                                <div class="post-meta">
                                    <span>by <a href="#">Admin</a></span>
                                    <span>12 Nov, 2013</span>
                                    <span>in <a href="#">Web Design</a></span>
                                </div>
                            </div>
                        </article>
                    </div>
                </div-->
            </article>
            <!--div id="respond" class="comment-respond">
                <h3 class="font-normal">Post A Comment</h3>
                <form class="comment-form">
                    <div class="row">
                        <div class="col-sm-4 form-group">
                            <input type="text" class="input-text full-width" placeholder="Full Name">
                        </div>
                        <div class="col-sm-4 form-group">
                            <input type="text" class="input-text full-width" placeholder="Email Address">
                        </div>
                        <div class="col-sm-4 form-group">
                            <input type="text" class="input-text full-width" placeholder="Your Website">
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea rows="8" class="input-text full-width" placeholder="Comment Type here"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn style1">Submit Message</button>
                    </div>
                </form>
            </div-->
        </div>
    </div>

        <!--div class="row">
            <div class="col-xs-12">
                <div class="blog-comments">
                    <hr/>
                    <button type="submit" class="btn btn-raised btn-info btn-lg">Comments <span class="raiseUp">{ {count($page_comments)}} </span></button>
                    { {form::open('blog/comment')}}
                    <input type="hidden" name="blog_id" value="{ {$page->id}}">
                    <div class="col-xm-12 well">
                        <textarea name="blog_comment" class="form-control" placeholder="Leave your comment here..." rows="2"></textarea>
                        <div class="text-right"><button type="submit" class="btn btn-raised btn-warning">Submit</button></div>
                    </div>
                    { {form::close()}}
                    <div class="row comment-section">
                        foreach ($page_comments as $value) { ?>
                        <div class="old-comments col-xs-12">
                            <div class="avatar col-md-1 hidden-xs"><img src="base_url('user-avatar/avatar-placeholder.jpg')"></div>
                            <div class="comment-body-section col-xs-12 col-md-11">
                                <h5>{ {$value->author->name;?> <small class="pull-right"><em> Posted: date('D, d M Y', strtotime($value->comment_date)) </em></small></h5>
                                <blockquote>
                                    { {$value->comment_body}}
                                </blockquote>
                            </div>
                        </div>
                        endforeach
                    </div>
                </div>
            </div><!--/.col-md-10-->
        <!--/div><!--/.row-->
@endsection

@push('script')
    <!-- waypoint -->
    <script type="text/javascript" src="{{ asset('js/waypoints.min.js') }}"></script>

    <!-- Owl Carousel -->
    <script type="text/javascript" src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}"></script>

    <!-- Media Element -->
    <script type="text/javascript" src="{{ asset('vendor/mediaelement/mediaelement-and-player.js') }}"></script>

    <!-- plugins -->
    <script type="text/javascript" src="{{ asset('js/jquery.plugins.js') }}"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
@endpush
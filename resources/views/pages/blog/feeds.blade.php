@extends('layouts.rss')

@section('content')
    @foreach($posts as $post)
        <item>
            <title>{{ $post->article_title }}</title>
            <category>{{ $post->category->name}}</category>
            <pubDate>{{ $post->created_at }}</pubDate>
            <author>{{ $post->custom_author or $post->author->name }}</author>
            <description>{{ $post->excerpt }}</description>
            <link>{{ route('posts.show', $post) }}</link>
        </item>
    @endforeach
@endsection
@extends('layouts.page')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.carousel.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.transitions.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/mediaelement/mediaelementplayer.min.css') }}" media="screen" />
    <!-- Magnific Popup core CSS file -->
    <link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}">
@endpush
@push('headerbottom')
    <div class="page-title-container style7">
        <div class="banner parallax" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="caption-wrapper position-left">
                    <div class="caption">
                        <h2 class="caption-lg">Business blog consisting of amazingly relevant and useful regularly update</h2>
                        <h5 class="caption-sm">tips, comparisons, and step-by-step guides to help its readers make informed, educated decisions - Loggcity Limited</h5>
                    </div>
                </div>
            </div>
        </div>
        <ul class="breadcrumbs">
            <li><a href="{{url('/')}}">Home</a></li>
            <li><a href="#">Company</a></li>
            <li class="active">{{ $page['title'] }}</li>
        </ul>
    </div>
@endpush
@section('content')
    <div class="container">
        <div id="main">
            <div class="blog-posts layout-timeline layout-fullwidth">
                <div class="timeline-author">
                    <img src="http://placehold.it/100x100" alt="">
                </div>
                <div class="iso-container iso-col-2 style-masonry has-column-width">
                @isset($posts)
                    @foreach ($posts as $post)
                    <div class="iso-item">
                        <article class="post post-masonry">
                            <div class="post-date">{{date("d M, Y", strtotime($post->created_at))}}</div>
                            @isset($post->image)
                                <div class="post-image">
                                    <div class="image">
                                        <img src="{{asset('storage/'.$post->image)}}" alt="{{$post->title}}" title="{{$post->title}}" />
                                        <div class="image-extras">
                                            <a href="#" class="post-gallery"></a>
                                        </div>
                                    </div>
                                </div>
                            @endisset
                            @empty($post->image)
                                <div class="post-image">
                                    <div class="image">
                                        <img src="http://placehold.it/780x390"  alt="{{$post->title}}" title="{{$post->title}}" >
                                        <div class="image-extras">
                                            <a href="#" class="post-gallery"></a>
                                        </div>
                                    </div>
                                </div>
                            @endempty
                            @isset($post->excerpt)
                            <div class="post-content no-author-img">
                                <div class="post-meta">
                                    <span class="entry-author fn">By <a href="#">{{$post->custom_author or $post->author->name}}</a></span>
                                    <span class="entry-time"><span class="published">{{date("d M, Y", strtotime($post->created_at))}}</span></span>
                                    <span class="post-category">in <a href="javascript:void(0)">{{$post->category->name or 'Uncategories'}}</a></span>
                                </div>
                                <h3 class="entry-title"><a href="{{route('posts.show', $post)}}">{{$post->article_title}}</a></h3>
                                <p>{{$post->excerpt}}</p>
                            </div>
                            @endisset
                            <div class="post-action">
                                <a href="#" class="btn btn-sm style3 post-comment"><i class="fa fa-comment"></i>25</a>
                                <a href="#" class="btn btn-sm style3 post-like"><i class="fa fa-heart"></i>480</a>
                                <a href="#" class="btn btn-sm style3 post-share"><i class="fa fa-share"></i>Share</a>
                                <a href="{{route('posts.show', $post)}}" class="btn btn-sm style3 post-read-more">More</a>
                            </div>
                        </article>
                    </div>
                    @endforeach
                @endisset
                </div>
                <a href="#" class="load-more"><i class="fa fa-angle-double-down"></i></a>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <!-- waypoint -->
    <script type="text/javascript" src="{{ asset('js/waypoints.min.js') }}"></script>

    <!-- Owl Carousel -->
    <script type="text/javascript" src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}"></script>

    <!-- carouFredSel -->
    <script type="text/javascript" src="{{ asset('vendor/carouFredSel-6.2.1/jquery.carouFredSel-6.2.1.js') }}"></script>

    <!-- Media Element -->
    <script type="text/javascript" src="{{ asset('vendor/mediaelement/mediaelement-and-player.js') }}"></script>

    <!-- plugins -->
    <script type="text/javascript" src="{{ asset('js/jquery.plugins.js') }}"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
@endpush
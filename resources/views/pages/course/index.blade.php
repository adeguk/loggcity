@extends('layouts.page')

@section('content')
    <section class="mbr-section" style="background:url({{asset('images/centre-1.jpg')}}) no-repeat 90% 45%;height: 270px;">
        <div class="container"></div>
    </section>

    <section class="mbr-section mbr-section-md-padding" >
        <div class="container-fluid">
            <!--div class="row">
                <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                    <div class="btn-group pull-right">
                        <a href="course/index" class="btn btn-raised btn-sm btn-default">All</a>
                        <a href="course/courses_type/paid" class="btn btn-raised btn-sm btn-warning">Paid</a>
                        <a href="course/courses_type/free" class="btn btn-raised btn-sm btn-primary">Free</a>
                    </div>
                </div>
            </div-->
            <div class="row">
                <div class="col-xs-12">
                    @isset($courses)
                        @foreach ($courses as $course)
                                <div class="col-lg-3 col-md-4 col-xs-12 col-sm-6 exam-item">
                                    <div class="card">
                                        <a href="{{route('courses.show', $course)}}">
                                            <div class="card-image">
                                                @if (isset($course->image))
                                                    <img class="card-img-top" src="{{asset('storage/' . $course->image)}}" data-src="holder.js/300x300" alt="{{$course->title}}" >
                                                @else
                                                    <img class="card-img-top" src="{{asset('images/placeholder.png')}}" data-src="holder.js/300x300" alt="{{$course->title}}" >
                                                @endif
                                            </div>
                                            <div class="card-body">
                                                <span class="text-danger" style="font-size: 14px;">{{ $course->subcategory->category->name .'/'.$course->subcategory->name }}</span><br/>
                                                <span class="card-title">{{ $course->title }}</span>
                                                <div class="card-action" style="padding:0; margin-top:15px; margin-bottom:-10px;">
                                                    <button class="btn btn-raised btn-primary">Start</button>
                                                    <!--button class="btn btn-raised btn-info">videos
                                                        <span class="raiseUp"></span>
                                                    </button-->
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div><!--/.col-md-4-->
                        @endforeach
                    @endisset
                </div>

                <div class="col-xs-12">
                    {!! $courses->render('vendor.pagination.myStyle') !!} </div>
            </div><!-- /.row End-->
        </div>
    </section><!--/#services-->
@endsection
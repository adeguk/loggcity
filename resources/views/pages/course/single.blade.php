@extends('layouts.page')

@section('content')
    <section class="mbr-section mbr-section-md-padding red darken-4" style="height: 270px;">
        <div class="container" id="course">
            <div class="row">
                <div class="col-xs-12 col-md-8 white-text">
                    <h4 style="margin-bottom: 40px;">
                        <strong>{{ $page->subcategory->category->name .'/'.$page->subcategory->name }}</strong>
                    </h4>
                    <p style="width:80%">{{ $page->excerpt }}</p>
                <!-- Course Review
                <p>
                    <img src="< ?=base_url('datafile/course-images/CPkGq6G.png')?>"/>
                    <img src="< ?=base_url('datafile/course-images/CPkGq6G.png')?>"/>
                    <img src="< ?=base_url('datafile/course-images/CPkGq6G.png')?>"/>
                    <img src="< ?=base_url('datafile/course-images/CPkGq6G.png')?>"/>
                    <img src="< ?=base_url('datafile/course-images/CPkGq6G.png')?>"/>
                    {{ $page->course_count_reviews }} Reviews
                </p><hr/>
                -->
                </div>
                <div class="col-xs-12 col-md-4">
                    @if (isset($page->image))
                        <img class="card-img-top course-summary-thumbnail" width="100%" src="{{asset('storage/' . $page->image)}}" data-src="holder.js/300x300" alt="{{$page->title}}" >
                    @else
                        <img class="card-img-top course-summary-thumbnail" width="100%" src="{{asset('images/placeholder.png')}}" data-src="holder.js/300x300" alt="{{$page->title}}" >
                    @endif
                </div>
            </div>
        </div>
    </section>

    <section id="course_detail" class="mbr-section">
        <div class="container">
            <div class="row">
                <div class="col-md-9" style="border-right: 1px solid #ccc;">
                    <!--div class="row">
                        <div class="col-xs-10 col-xs-offset-1">
                            validation and status message
                        </div>
                    </div-->
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <h4><strong>Who is it for?</strong></h4>
                            <div class="extraln">{!! $page->audience  or 'Anyone with interest can take this course' !!}</div>

                            <h4><strong>What is it about?</strong></h4>
                            <div class="extraln">{!! $page->description or 'To be completed?' !!}</div>

                            <h4><strong>What will I get from this course?</strong></h4>
                            <div class="extraln">{!! $page->what_i_get or '' !!}</div>

                            <h4><strong>Course requirement</strong></h4>
                            <div class="extraln">{!! $page->requirement or '' !!}</div>
                        </div>
                        <div class="col-xs-12 col-md-6 deep-orange darken-4 text-white" style="border-radius:10px 0 0 10px;">
                            <h4 style="text-align:center;padding-top: 15px;"><strong>Lesson Overview</strong></h4>
                            <div class="extraln">
                                {!! $page->course_overview or 'No outline available!' !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="pb-t">
                        <div class="pb-p">
                            <span class="pb-pr ">
                            @if ($page->price)
                                <h3><small>Cost:</small> <strong>{{ $currency_symbol }}</strong>{{ $page->price }}</h3>
                            @else
                                <h3><small>Cost:</small> <strong>Free</strong></h3>
                            @endif
                            </span>
                        </div>
                        <div class="pb-ta">
                            @guest
                                <a href="{{ route('login') }}" class="btn btn-raised btn-primary"> Login to Enroll </a>
                            @endguest
                        </div>
                    </div>

                    <div class="big-gap">&nbsp;</div>

                    <div>
                        <h4 class="related_courses">Related courses: </h4>
                        <div class="big-gap">&nbsp;</div>
                    </div>

                    @foreach ($related_courses as $value)
                        @if ($value->id != $page->id)
                        <div class="thumbnail">
                            <a href="{{ route('courses.show', $value->slug) }}">
                                @if (isset($value->image))
                                    <img height="100%" src="{{asset('storage/' . $value->image)}}" data-src="holder.js/300x300" alt="{{$value->title}}" >
                                @else
                                    <img height="100%" src="{{asset('images/placeholder.png')}}" data-src="holder.js/300x300" alt="{{$value->title}}" >
                                @endif

                                <div class="caption">
                                    <p>{{ $value->title }}</p>
                                </div>
                            </a>
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </section><!--/#pricing-->
@endsection
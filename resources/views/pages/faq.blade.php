@extends('layouts.page')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/jquery.sky.carousel/skin-variation.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.carousel.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.transitions.css') }}" media="screen" />
    <!-- Magnific Popup core CSS file -->
    <link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}">
@endpush

@push('headerbottom')
    <div class="page-title-container style7">
        <div class="banner parallax" data-stellar-background-ratio="0.5">
            <div class="container">
                <div class="caption-wrapper position-left">
                    <div class="caption">
                        <h2 class="caption-lg">{{ $page['title'] }}</h2>
                        <h5 class="caption-sm">Frequently Asked Question</h5>
                    </div>
                </div>
            </div>
        </div>
        <ul class="breadcrumbs">
            <li><a href="{{url('/')}}">Home</a></li>
            <li><a href="#">Resources</a></li>
            <li class="active">{{ $page['title'] }}</li>
        </ul>
    </div>
@endpush

@section('content')
        <div class="container ">
            <div class="row">
                <div class="col-md-4 col-xs-12 wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                    <h3 class="page-header fw3" style="margin-top: 0px;">Most asked questions</h3>
                    <p class="para">We are one-stop solution for IT related services. At Loggcity, we handle complexities,
                        expectional specifications and challenges with great care, passion and confidence.
                    </p>
                    <p class="para">Our people are equipped with dynamic proficiency and our expertise lies in providing
                        outstanding services to our clients.</p>
                </div>
                <div class="col-md-8 col-xs-12 wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                    <div role="tablist" id="accordion" class="panel-group my-accordion about-us">
                    @isset($faqs)
                        @foreach ($faqs as $faq)
                        <div class="panel panel-default">
                            <div id="{{$faq->id}}" role="tab" class="panel-heading">
                                <h4 class="panel-title">
                                    <a aria-controls="collapse{{$faq->id}}" aria-expanded="false"
                                       href="#collapse{{$faq->id}}" data-parent="#accordion" data-toggle="collapse">
                                        <span style="font-weight:normal;">{{$faq->faq_group->name}}: </span>{{ $faq->faq_question}}
                                    </a>
                                </h4>
                            </div>
                            <div aria-labelledby="{{$faq->id}}" role="tabpanel"
                                 class="panel-collapse collapse" id="collapse{{$faq->id}}">
                                <div class="panel-body">
                                    {!! $faq->faq_answer !!}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @endisset
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('script')
    <!-- waypoint -->
    <script type="text/javascript" src="{{ asset('js/waypoints.min.js') }}"></script>

    <!-- Owl Carousel -->
    <script type="text/javascript" src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jquery.sky.carousel/jquery.sky.carousel-1.0.2.js') }}"></script>

    <!-- plugins -->
    <script type="text/javascript" src="{{ asset('js/jquery.plugins.js') }}"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
@endpush
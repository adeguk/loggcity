@extends('layouts.page')

@section('content')
    <section class="mbr-section" style="background:url({{asset('images/slider/mpumelelo-macu-283883.jpg')}}) no-repeat;height: 545px;">
        <div class="container white-text">
            <div class="row">
                <div class="col-lg-6" style="background:rgba(0,0,0,0.25)">
                    <p class="para"><strong>Loggcity various divisions, though from different industrial sectors, had find a way
                        to connect themselves seemlessly in an unusual way. At the same time, the close working relations
                        that exist throughout the group provide a healthy and constant exchange of knowledge and ideas. Thereby
                        ensure we always develop the best and most appropriate services and products for each and every customers,
                            utilizing and combining the necessary skills across the divisions.</strong>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="mbr-section mbr-section-md-padding" style="padding-top:0;margin-top:-90px">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-md-6">
                        <div class="card blue-grey darken-4">
                            <h4 class="card-header">Capide</h4>
                            <div class="card-body white-text">
                                <h4 class="card-title"><strong class="orange-text">Prince F.O. Adegoroye</strong>, <small>VP & Division Head</small></h4>
                                <p class="card-text">
                                    Capide is one of the leading independent construction, development and property management firms in Nigeria;
                                     a nationwide contractor specialising in maximising value for customers in
                                    the public and private sector, with an uncompromising focus on safety and quality.
                                </p>
                                <p>
                                    Our focus includes building and civil engineering projects management, blocks and bricks making,
                                    painting and interior decoration, property caretaker and maintenance.
                                </p>
                                <!--a href="#" class="btn btn-success white-text">Go somewhere</a-->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card blue-grey darken-4">
                            <h4 class="card-header">Lumiverse: Agro-Division</h4>
                            <div class="card-body white-text">
                                <h4 class="card-title"><strong class="orange-text">Prince A.A. Adegoroye</strong>, <small>CEO & Division Head</small></h4>
                                <p class="card-text">
                                    Lumiverse is a Nigerian farming and food products business engage in general farming (fishery, poultry, pig farming
                                     and crop farming) sales and supply of agricultural products, tools and machinery.
                                </p>
                                <p>We attention to every detail ensuring
                                    our customers are confident they have the best product for their needs.
                                    Utilising skills from across the divisions, Lumiverse is set to driving innovation
                                    in modern agriculture to combat food crisis and Climate Change.
                                </p>
                                <!--a href="#" class="btn btn-success white-text">Go somewhere</a-->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 20px"><blockquote class="para text-center">Data and technology are at the heart of what we do.</blockquote> </div>
                    <div class="col-md-6">
                        <div class="card blue-grey darken-4">
                            <h4 class="card-header">Digital Division</h4>
                            <div class="card-body white-text">
                                <h4 class="card-title"><strong class="orange-text">Prince A.A. Adegoroye</strong>, <small>CEO & Division Head</small></h4>
                                <p class="card-text">
                                    Loggcity Digital is a creative and digital market agency based in Akure (Ondo State, Nigeria) and Preston (Lancashire, UK)
                                    who specialise in the design and implementation of creative and integrated marketing campaigns through a variety of media channels and services.
                                </p>
                                <p class="card-text">
                                    Our Digital division is passionate about simple yet sophisticated user experience and designs that our clients are very proud of.
                                    We represent candidates in strategy and creative backgrounds on both permanent and contract positions on-site and in-house.
                                </p>
                                <a href="http://digital.loggcity.uk" class="btn btn-success white-text">Learn More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card blue-grey darken-4">
                            <h4 class="card-header">iKnow: Education Division</h4>
                            <div class="card-body white-text">
                                <h4 class="card-title"><strong class="orange-text">Deaconess C.T. Adegoroye</strong>, <small>VP & Division Head</small></h4>
                                <p class="card-text">
                                    Committed to providing top rate quality services with high quality
                                    materials including case studies, checklist and examples.
                                </p>
                                <a href="/courses" class="btn btn-success white-text">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
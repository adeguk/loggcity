@extends('layouts.page')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/animate/animate.min.css') }}"> 
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/jquery.sky.carousel/skin-variation.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.carousel.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.transitions.css') }}" media="screen" />
    <!-- Magnific Popup core CSS file -->
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}"> 
@endpush

@push('headerbottom')
<div class="page-title-container">
    <div class="soap-google-map"></div>
</div>
@endpush

@section('content')
        <div class="container">
            <div class="contact-address style3 col-md-11 col-xs-12">
                <div class="heading-box info">
                    <h2 class="box-title">Just ask. Get answers.</h2>
                    <p class="desc-md">Your questions and comments are important to us. Reach us by phone or email
                        or click the button below to visit our support centre.</p>
                    <p>We are always available to help you with any of your business needs and we make every effort to
                        ensure contacting us is as easy as possible. If you have urgent question, we have
                        got answers to some frequently asked questions about our services and much more.</p>
                </div>               
            </div>
        </div>
        <div class="container">
                <div class="row">
                    <div class="col-sm-8 box">
                    @if(Session::has('success'))
                        <div class="alert alert-success" data-animation-type="fadeOut" data-animation-duration="3" data-animation-delay="0">
                            {{ Session::get('success') }}
                            <span class="close"></span>
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert alert-error" data-animation-type="fadeOut" data-animation-duration="1" data-animation-delay="0">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <span class="close"></span>
                        </div>
                    @endif

                        {!! Form::open(['route' => 'pages.sendContactMail', 'method' => 'post', 'class' => 'validate', 'role' => 'form']) !!}                            
                            <div class="form-group dropdown">
                                <select name="subject" class="selector full-width">
                                    <option value="">Select Subject Matter</option>
                                    <option value="General Enquiry">General Enquiry</option>
                                    <option value="Suggestion">Suggestion</option>
                                    <option value="Quick Support">Quick Support</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="form-group col-sms-6 col-sm-6">
                                    {!! Form::text('fullname', null, ['placeholder'=>'Full name','class'=>'input-text full-width' ]) !!}
                                </div>
                                <div class="form-group col-sms-6 col-sm-6">
                                    {!! Form::text('company', null, ['placeholder'=>'Company','class'=>'input-text full-width' ]) !!}
                                </div>
                                <div class="form-group col-sms-6 col-sm-6">
                                    {!! Form::email('email', null, ['placeholder'=>'Email address','class'=>'input-text full-width' ]) !!}
                                </div>
                                <div class="form-group col-sms-6 col-sm-6">
                                    {!! Form::text('phone', null, ['placeholder'=>'Telephone','class'=>'input-text full-width' ]) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                {!! Form::textarea('body', null, ['placeholder'=>'Enter Message','role'=>'10','class'=>'input-text full-width' ]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Send Message', ['class' => 'btn btn-md style1']) !!}
                                {!! Form::reset('Clear', ['class' => 'btn btn-md style2']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-sm-4">
                        <div class="tab-container full-width style2 box">
                            <ul class="tabs clearfix">
                                <li class="active"><a href="#london" data-toggle="tab">London, UK</a></li>
                                <li><a href="#akure" data-toggle="tab">Akure, Nigeria</a></li>
                                <li><a href="#india" data-toggle="tab">India</a></li>
                            </ul>
                            <div id="london" class="tab-content in active">                                    
                                <div class="tab-pane">
                                    <ul class="contact-address style1">
                                        <li class="office-address">
                                            <i class="fa fa-map-marker"></i>
                                            <div class="details">
                                                <p>88 Peckham Rye, London SE15 4HA. UK</p>
                                            </div>
                                        </li>
                                        <li class="phone">
                                            <i class="fa fa-phone"></i>
                                            <div class="details">
                                                <h5>Phone</h5>
                                                <p>
                                                    Global: +44 207 732 7411
                                                    <br>
                                                    Hotline: +44 743 532 0677
                                                </p>
                                            </div>
                                        </li>
                                        <li class="email-address">
                                            <i class="fa fa-envelope"></i>
                                            <div class="details">
                                                <p>
                                                    info@loggcity.co.uk
                                                    <br>
                                                    abuse@loggcity.co.uk
                                                    <br>
                                                    legals@loggcity.co.uk
                                                    <br>
                                                    sales@loggcity.co.uk
                                                </p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>           
                            <div id="akure" class="tab-content">
                                <div class="tab-pane">
                                    <ul class="contact-address style1">
                                        <li class="office-address">
                                            <i class="fa fa-map-marker"></i>
                                            <div class="details">
                                                <p>Suite 3, Yafrato Complex, Alagbaka, Akure, Ondo State. Nigeria</p>
                                            </div>
                                        </li>
                                        <li class="phone">
                                            <i class="fa fa-phone"></i>
                                            <div class="details">
                                                <p>
                                                    Global: +44 207 732 7411
                                                    <br>
                                                    Hotline: +44 743 532 0677
                                                </p>
                                            </div>
                                        </li>
                                        <li class="email-address">
                                            <i class="fa fa-envelope"></i>
                                            <div class="details">
                                                <p>
                                                    info@loggcity.co.uk
                                                    <br>
                                                    abuse@loggcity.co.uk
                                                </p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div id="india" class="tab-content">
                                <div class="tab-pane">
                                    <ul class="contact-address style1">
                                        <li class="office-address">
                                            <i class="fa fa-map-marker"></i>
                                            <div class="details">
                                                <p>1st Block, Basaveshwara Nagar, Bangalore 560079 India</p>
                                            </div>
                                        </li>
                                        <li class="phone">
                                            <i class="fa fa-phone"></i>
                                            <div class="details">
                                                <p>
                                                    Global: +44 207 732 7411
                                                    <br>
                                                    Hotline: +44 743 532 0677
                                                </p>
                                            </div>
                                        </li>
                                        <li class="email-address">
                                            <i class="fa fa-envelope"></i>
                                            <div class="details">
                                                <p>
                                                    info@loggcity.co.uk
                                                    <br>
                                                    abuse@loggcity.co.uk
                                                    <br>
                                                    legals@loggcity.co.uk
                                                    <br>
                                                    sales@loggcity.co.uk
                                                </p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <div class="social-icons style1 size-md">
                            <a href="https://twitter.com/InfoLoggcity" class="social-icon" target="_blank" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter has-circle"></i></a>
                            <a href="https://facebook.com/Loggcity" class="social-icon" target="_blank" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook has-circle"></i></a>
                            <a href="#" class="social-icon" target="_blank" data-toggle="tooltip" data-placement="top" title="GooglePlus"><i class="fa fa-google-plus has-circle"></i></a>
                            <a href="https://www.linkedin.com/company/loggcity-ltd" class="social-icon" target="_blank" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin has-circle"></i></a>
                            <a href="https://www.pinterest.co.uk/loggcitydigi/pins/" class="social-icon" target="_blank" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest has-circle"></i></a>
                        </div>
                    <!--a class="btn btn-raised btn-primary" href="http://gateway.loggcity.uk/knowledge-base">Check out our knowledge centre for more</a-->
                    </div>
                </div>
            </div>

@endsection


@push('script')
    <!-- Google Map Api -->
    <script type='text/javascript' src="http://maps.google.com/maps/api/js?key=AIzaSyA0Dx_boXQiwvdz8sJHoYeZNVTdoWONYkU&amp;language=en"></script>
    <script type="text/javascript" src="{{ asset('js/gmap3.js') }}"></script>
    <!-- waypoint -->
    <script type="text/javascript" src="{{ asset('js/waypoints.min.js') }}"></script>

    <!-- Owl Carousel -->
    <script type="text/javascript" src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jquery.sky.carousel/jquery.sky.carousel-1.0.2.js') }}"></script>

    <!-- plugins -->
    <script type="text/javascript" src="{{ asset('js/jquery.plugins.js') }}"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
    <script type="text/javascript">
        sjq(".soap-google-map").gmap3({
            map: {
                options: {
                    center: [30.00000,7.00000],
                    zoom: 3.25,
					mapTypeControlOptions: {
						position: google.maps.ControlPosition.RIGHT_BOTTOM
					},
					zoomControlOptions: {
						position: google.maps.ControlPosition.LEFT_CENTER
					},
					panControlOptions: {
						position: google.maps.ControlPosition.LEFT_CENTER
					}
                }
            },
            marker:{
                values: [
                    {latLng:[51.464234,-0.06888], data:"United Kingdom"},
                    {latLng:[7.25283,5.21046], data:"Nigeria"},
                    {latLng:[12.99957,77.53114], data:"India"}
                ],
                options: {
                    draggable: false,
                    icon: "images/icon/marker.png",
                },
            }
        });
    </script>
@endpush
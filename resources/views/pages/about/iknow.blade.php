@extends('layouts.page')

@section('content')
    <section class="mbr-section mbr-section-md-padding white">
        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <p class="para">Our Educational and Training Centre (ETC) referred to as <strong>"iKnow"</strong> provide a
                        unique learning and development opportunities for individuals and Corporate bodies nationwide.
                        We worked with Local Councils around Kent (from Medway Centre), Devon area and Local Government
                        Areas in Nigeria. Candidates are referred to us for our training courses from various clients including  the DWP. </p>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="page-header fw4">Benefits of attending our academy</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <ul class="features about-us teal darken-4 white-text ">
                        <li>Competitive low cost</li>
                        <li>Corporate Client will have the ability to track their employees
                            progress and receive corporate discounts.</li>
                        <li>Committed to providing top rate quality services with high quality
                            materials including case studies, checklist and examples</li>
                        <li>Courses held all year round, at nationwide locations and online for
                            maximum flexibility</li>
                    <!--/ul>
                </div>
                <div class="col-md-6 col-xs-12">
                    <ul class="features-list about-us"-->
                        <li>Meet colleagues working in similar environments at other organisations</li>
                        <li>
                            Temporary accommodation for candidates attending our training courses which be 1-day course to 4-month in  duration</li>
                        <li>Up to 1 year post training support, improve your skills,
                            share good practice and learn from others</li>
                        <li>Availability of upto 4 weeks work placement for young people
                            aged 16-24 undertaking a 10-week programme with one of our partners </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="para">Our distant candidate benefits from study bedroom with shower and kitchen facilities and
                        all the general amenities they might expect. Friendly staff and a warm atmosphere throughout
                        make this a very “home from home” environment in which to work and relax.</p>
                    <div class="col-xs-12 center">
                    @guest
                        <a href="{{ route('login') }}" class="btn btn-raised btn-default btn-lg login_open">Login</a>
                        <a href="{{ route('register') }}" class="btn btn-raised btn-warning btn-lg register_open">Register</a>
                    @endguest
                        <a href="#" class="btn btn-raised btn-primary btn-lg">Browse Our Courses</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
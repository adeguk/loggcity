@extends('layouts.page')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/jquery.sky.carousel/skin-variation.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.carousel.css') }}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.transitions.css') }}" media="screen" />
    <!-- Magnific Popup core CSS file -->
    <link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}"> 
@endpush

@push('headerbottom')
<div class="page-title-container companyBanner">
    <div class="banner parallax" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="caption-wrapper position-left">
                <div class="caption">
                    <h2 class="caption-lg">Tradition of professional service with a promise of originality.</h2>
                    <h5 class="caption-sm">Converting Business Ideas to Solutions your Customer Love - Loggcity Limited</h5>
                </div>
            </div>
        </div>
    </div>
    <ul class="breadcrumbs">
        <li><a href="{{url('/')}}">Home</a></li>
        <li><a href="#">Company</a></li>
        <li class="active">{{ $page['title'] }}</li>
    </ul>
</div>
@endpush

@section('content')
    <div class="section">
        <div class="container">
            
            <div class="section-info">
                <h3 class="section-title">A little about Loggcity</h3>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <p><span class="dropcap style1">T</span>he organisation began operations in 2005 as Adeguk Limited, providing graphic design solution to its local community. 
                        It soon became a centre of excellence as more customers turned to us for more services such as Web design and development, training and network solution. 
                        The organisation became a corporation and registered as Loggcity Limited in England in 2009.</p>
                        <p>In 2015, Loggcity Limited commenced operating in Nigeria and became incorporate with CAC in 2016 to satisfy customer requirements for quality and 
                        cost effective solutions services in and around Lagos area. This business has developed well and is gradually expanding successfully.</p>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <blockquote class="style3">
                            <p>From the corporate ID for SME to the new challenges of the global market: Loggcity Limited has become a solution-oriented ICT/IS service provider. 
                        It is a business dedicated to capacity building for next-generation of business enterprise. It is located in the business district of Nigeria with business 
                        arms in United Kingdom and India.</p>
                        </blockquote>
                    </div>
                </div>
            </div>
            <div class="section-info">            
                <h3 class="section-title">Our Divisions</h3>
                <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="shortcode-banner style-animated">
                        <img src="{{asset('images/pages/company/capide-loggcity-construction-division-men-at-work.jpg')}}" alt="capide:loggcity construction division, men at work">
                        <div class="shortcode-banner-inside">
                            <div class="shortcode-banner-content">
                                <h3 class="banner-title">Construction: Capide</h3>
                                <div class="details">
                                    <p>Capide specialises in maximising value for customers in the public and private sector, with an uncompromising focus on safety 
                                        and quality. 
                                        Our focus includes civil engineering projects management, blockmaking, property 
                                        caretaker and maintenance.</p>
                                </div>
                                    <a href="javascript:void(0)" class="btn style5 btn-sm">More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="shortcode-banner style-animated">
                        <img src="{{asset('images/pages/company/lumiverse-loggcity-maize-farming-at-owo-akure-express.jpg')}}" alt="lumiverse loggcity maize farming at owo akure express">
                        <div class="shortcode-banner-inside">
                            <div class="shortcode-banner-content">
                                <h3 class="banner-title">Agro-Division</h3>
                                <div class="details">
                                    <p>Lumiverse engage in general farming (fishery, poultry, piggery &amp; crop farming) sales, supply of agricultural products, tools and machinery.
                                        Utilising skills from across the divisions, we are set to driving innovation in modern agriculture to combat food crisis and Climate Change.</p>
                                </div>
                                    <a href="javascript:void(0)" class="btn style5 btn-sm">More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="shortcode-banner style-animated">
                        <img src="{{asset('images/pages/company/loggcity-digital-service-division.jpg')}}" alt="loggcity digital service division">
                        <div class="shortcode-banner-inside">
                            <div class="shortcode-banner-content">
                                <h3 class="banner-title">Digital Division</h3>
                                <div class="details">
                                    <p>Our Digital division is passionate about simple yet sophisticated user experience and designs that our clients are very proud of. 
                                        We represent candidates from various industries on both permanent and contract positions on-site and in-house.</p>
                                </div>
                                    <a href="https://digital.loggcity.uk/about-us" class="btn style5 btn-sm">More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

    @include('partials.component.why-us')
    <div class="callout-box style3">
        <div class="container">
            <div class="callout-content">
                <div class="callout-text">
                    <h2>True partnership Loggcity will make your business in grow again!</h2>
                </div>
                <div class="callout-action">
                    <a class="btn style3" href="{{url('/getting-in-touch')}}">Get in Touch</a>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="container">
            <!--div class="heading-box">
                <h2 class="box-title">Miracle Animated Infographic Pies</h2>
                <p class="desc-lg">Let some skills rotate and get noticed :)</p>
            </div-->
            <div class="row">
                <div class="col-sx-6 col-md-12">
                <div class="tab-container full-width style2">
                                        <ul class="tabs clearfix">
                                            <li class="active"><a href="#mission" data-toggle="tab">Mission and Vision</a></li>
                                            <li><a href="#1-team" data-toggle="tab">One Team</a></li>
                                            <li><a href="#csr" data-toggle="tab">Corporate Social Responsibility</a></li>
                                        </ul>
                                        <div id="mission" class="tab-content in active">
                                            <div class="tab-pane">
                                                <img src="http://placehold.it/270x237" alt="" class="pull-left">
                                                <p>We not only want to provide affordable services, but to provide accessible, informative and streamlined IT solution to businesses of all 
                                                    size nationwide. On-going service system development offering many different types of web and print solution that is exclusively 
                                                    tailor to fit the needs and resources of small and medium-size companies.</p>
                                                <p>Loggcity seeks to provide these services in the timeliest manner and with an on-going comprehensive quality control programme to provide 
                                                    100% customer satisfaction. The company's key officers see each contract as an agreement not between a business and its customers, 
                                                    but between partners that wish to create a close and mutually beneficial long-term relationship.</p>
                                            </div>
                                        </div>
                                        <div id="1-team" class="tab-content">
                                            <div class="tab-pane">
                                                <blockquote class="style1 box">
                                                    <p>The people that make Loggcity a leading edge when it comes to solution-based approaches.</p>
                                                </blockquote>
                                                <p>As befits an organization of our reach and reputation, the Loggcity senior management team is made up of internationally respected business 
                                                    leaders. We are a sincere company with a straightforward vision and we take our appetite for uniqueness very seriously. Our Senior Partners 
                                                    enables us to meet the key management and business challenges of the modern world - shaping government strategic policy, developing new 
                                                    solutions or implementing new technologies.</p>
                                                <p>As a company that takes great pride in providing excellent service, our Associates are our most valued assets and they continue to serve 
                                                    you with our vision and mission in mind. It is not just our clients who are assured of warm welcome; you can too. We offer flexible hours 
                                                    set around you and your lifestyle, accredited training and fantastic career prospect and a whole lot more.</p>
                                                <p>From time to time, we may require the services of skilled personnel on contract and full time basis. Send in your resume to info@loggcity.com.ng or info@loggcity.co.uk</p>
                                            </div>
                                        </div>
                                        <div id="csr" class="tab-content">
                                            <div class="tab-pane">
                                                <p>Our fundamental objective is realising how we influence the community that we operating in, knowing that we will stand the test of 
                                                    time if the local residents and other small business approve and support our business.</p>
                                                <p>Part of our goal, at Loggcity as a responsible corporate citizen is to build a sustainable business in a sustainable environment. 
                                                    In doing so, our CSR pay attention on creating sustainable value for our stakeholders and community, engaging only in fair practice and 
                                                    promote diversity while ensuring our people with rewarding career opportunity. We consider the social, environmental and governance issue 
                                                    that surrounds any ICT projects we undertake; positively contribute directly or indirectly to the academic, business and economic development
                                                    of the community we operate.</p>
                                                <p>It is part of our objective to integrate this into our daily operations and to sustain it as our way of conducting business. We will be a 
                                                    good neighbour to the businesses in our area, and we will be a contributing and supportive member of our community.</p>
                                            </div>
                                        </div>
                                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.component.yearly-insight')
@endsection

@push('script')
    <!-- waypoint -->
    <script type="text/javascript" src="{{ asset('js/waypoints.min.js') }}"></script>

    <!-- Owl Carousel -->
    <script type="text/javascript" src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/jquery.sky.carousel/jquery.sky.carousel-1.0.2.js') }}"></script>

    <!-- plugins -->
    <script type="text/javascript" src="{{ asset('js/jquery.plugins.js') }}"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
@endpush
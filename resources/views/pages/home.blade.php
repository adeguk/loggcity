<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="icon-box style-boxed-1 animated box" data-animation-type="fadeInUp" data-animation-delay="0.5">
                    <div class="icon-container">
                        <i class="fa fa-leaf"></i>
                    </div>
                    <div class="box-content">
                        <h4 class="box-title"><a href="#">Genuine Personality</a></h4>
                        <p>Loggcity is one of the contributors of IT business solution to variety of industry sectors in Africa and Europe.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="icon-box style-boxed-1 animated box" data-animation-type="fadeInUp" data-animation-delay="0.75">
                    <div class="icon-container">
                        <i class="fa fa-search"></i>
                    </div>
                    <div class="box-content">
                        <h4 class="box-title"><a href="#">Strong R&amp;D</a></h4>
                        <p>We offer an integrated service to promote business success and knowledge management in both private and public sector.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="icon-box style-boxed-1 animated box" data-animation-type="fadeInUp" data-animation-delay="1">
                    <div class="icon-container">
                        <i class="fa fa-tint"></i>
                    </div>
                    <div class="box-content">
                        <h4 class="box-title"><a href="#">Limitless Possibilities</a></h4>
                        <p>We strongly believe in the potential growth of any business and we're passionate about working alongside you to fulfil it.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('partials.component.yearly-insight')
<!-- post slideshow -->
@include('partials.component.showcase')
@include('partials.component.3-column-blog')
@include('partials.component.special-report')
@include('partials.component.callout-action')

<!--div class="parallax has-caption parallax-image2" data-stellar-background-ratio="0.5">
    <div class="caption-wrapper">
        <h2 class="caption animated size-lg" data-animation-type="fadeInLeft" data-animation-duration="2" data-animation-delay="0">Whatever We Do, We Give our Best!</h2>
        <br>
        <h3 class="caption animated size-md" data-animation-type="fadeInLeft" data-animation-duration="2" data-animation-delay="1">welcome to parallax scrolling</h3>
    </div>
</div-->
@include('partials.component.why-us')
<div class="responsive-section">
    <div data-stellar-background-ratio="0.5" class="callout-box style1 parallax parallax-image2 ">
        <div class="callout-box-wrapper no-bg">
            <div class="container">
                <div class="row same-height">
                    <div class="col-md-6 callout-content-container animated" data-animation-type="fadeInLeft">
                        <div class="callout-content">
                            <div class="heading-box">
                                <h2 class="box-title color-white"><strong>Loggcity Digital: Building Brand, Website and App with 360 Solution!</strong></h2>
                                <p>the digital arm of Loggcity Limited, specializes in web design and bespoke development, PPC advertising, eCommerce, hosting and optimization of 
                                business solutions that improve the effectiveness of digital marketing, product presentation, interactive transactions, customer relationship management
                                 and daily business procedures</p>
                                <p>We will develop your software to the highest professional standards using Best Practice methods; giving you enhanced reliability and improved data consistency.</p>
                            </div>
                            <div class="responsive-button">
                                <a href="#" class="btn-tablet-view"><i class="fa fa-tablet"></i>Tablet View</a><a href="#" class="btn-mobile-view active"><i class="fa fa-mobile"></i>Mobile View</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 animated" data-animation-type="fadeInRight">
                        <div class="callout-image-container">
                            <div class="callout-image hide-children">
                                <img class="responsive-tablet" alt="" src="{{ asset('images/pages/homepage/1/ipad.png') }}">
								<img class="responsive-mobile active" alt="" src="{{ asset('images/pages/homepage/1/iphone.png') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bg-client-section">
    <div class="section">
        <div class="container">
            <div class="heading-box">
                <h2 class="box-title color-white">Our Awesome Friends</h2>
                <p class="desc-lg skin-color">We work closely with a wide range of clients and partners from different sectors and regions. 
                Most of which rely on Loggcity for innovation and deep specialist expertise, delivered with the ease of full-service capability.</p>
            </div>
            <div class="overflow-hidden">
                <div class="brand-slider owl-carousel" data-items="4" data-itemsPerDisplayWidth="[[0, 1], [480, 1], [768, 2], [992, 3], [1200, 4]]">
                    <a href="http://uniteplus.co.uk/" target="_blank">
                        <img src="{{ asset('images/logo-slider/unite-plus.png') }}" alt="Uniteplus">
                    </a>
                    <a href="http://ec.europa.eu/regional_policy/en/funding/erdf/" target="_blank">
                        <img src="{{ asset('images/logo-slider/european-union.png') }}" alt="European Union">
                    </a>
                    <a href="http://thenexusltd.co.uk/" target="_blank">
                        <img src="{{ asset('images/logo-slider/nexus-exchange.png') }}" alt="The Nexus Exchange">
                    </a>
                    <a href="https://www.facebook.com/Motherlanders-278187786035397/" target="_blank">
                        <img src="{{ asset('images/logo-slider/motherlanders.png') }}" alt="Motherlanders">
                    </a>
                    <a href="https://easyprinter.com" target="_blank">
                        <img src="{{ asset('images/logo-slider/easyprinter.png') }}" alt="Easy Printer">
                    </a>
                    <a href="https://digital.loggcity.uk" target="_blank">
                        <img src="{{ asset('images/logo-slider/loggcity-digital.png') }}" alt="Loggcity Digital">
                    </a>
                    <a href="http://luebo.io" target="_blank">
                        <img src="{{ asset('images/logo-slider/luebo-technologies.png') }}" alt="Luebo Technologies">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
    <!-- waypoint -->
    <script type="text/javascript" src="{{ asset('js/waypoints.min.js') }}"></script>

    <!-- Owl Carousel -->
    <script type="text/javascript" src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}"></script>

    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="{{ asset('vendor/revolution_slider/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/revolution_slider/js/jquery.themepunch.revolution.min.js') }}"></script>

    <!-- plugins -->
    <script type="text/javascript" src="{{ asset('js/jquery.plugins.js') }}"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/revolution-slider.js') }}"></script>
@endpush
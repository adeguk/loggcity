@extends('layouts.appdemo')

@section('content')
    <section class="mbr-section" style="background:url({{asset('images/workinglate_1920.jpg')}}) no-repeat 0% 10%;height: 270px;">
        <div class="container wrapper"></div>
    </section>

    <section class="about-us-area mbr-section white" id="faq">
        <div class="container wrapper">
            <div class="row">
                <div class="col-md-4 col-xs-12 wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                    <h3 class="page-header fw3" style="margin-top: 0px;">Most asked questions</h3>
                    <p class="para">We are one-stop solution for IT related services. At Loggcity, we handle complexities,
                        expectional specifications and challenges with great care, passion and confidence.
                    </p>
                    <p class="para">Our people are equipped with dynamic proficiency and our expertise lies in providing
                        outstanding services to our clients.</p>
                </div>
                <div class="col-md-8 col-xs-12 wow fadeInUp" data-wow-delay="400ms" data-wow-duration="700ms">
                    <div role="tablist" id="accordion" class="panel-group my-accordion about-us">

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
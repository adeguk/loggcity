<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="this is a description">
    <meta name="keyword" content="key, keyword">
    <meta name="author" content="Adewale Adegoroye">
    @if(isset($page->title))
        <title>@yield('page_title', $page->title. " - " . setting('admin.description'))</title>
    @else
        <title>@yield('page_title', setting('site.title'). " - " . setting('admin.description'))</title>
    @endif
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('images/loggcity.ico') }}" type="image/x-icon">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('css')
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]> <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script> 
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> <![endif]-->
</head>
<body>
    @include('partials.top_navigation')

    @yield('content')

    @include('partials.footer')
    <!-- Scripts -->
    <script>
            $.material.init();
    </script>
</body>
</html>

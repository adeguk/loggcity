<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Test</title>

    <script src="https://use.fontawesome.com/31c23a3eac.js"></script>
    <style>
        @import url(http://fonts.googleapis.com/css?family=Roboto:300); /*Calling our web font*/

        /* Some resets and issue fixes */
        #outlook a { padding:0; }
        body{ width:100% !important; -webkit-text; size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; }
        .ReadMsgBody { width: 100%; }
        .ExternalClass {width:100%;}
        .backgroundTable {margin:0 auto; padding:0; width:100%;!important;}
        table td {border-collapse: collapse;}
        .ExternalClass * {line-height: 115%;}
        /* End reset */

        /* These are our tablet/medium screen media queries */
        @media screen and (max-width: 630px){
            /* Display block allows us to stack elements */
            *[class="mobile-column"] {display: block;}

            /* Some more stacking elements */
            *[class="mob-column"] {float: none !important;width: 100% !important;}

            /* Hide stuff */
            *[class="hide"] {display:none !important;}

            /* This sets elements to 100% width and fixes the height issues too, a god send */
            *[class="100p"] {width:100% !important; height:auto !important;}

            /* For the 2x2 stack */
            *[class="condensed"] {padding-bottom:40px !important; display: block;}

            /* Centers content on mobile */
            *[class="center"] {text-align:center !important; width:100% !important; height:auto !important;}

            /* 100percent width section with 20px padding */
            *[class="100pad"] {width:100% !important; padding:20px;}

            /* 100percent width section with 20px padding left & right */
            *[class="100padleftright"] {width:100% !important; padding:0 20px 0 20px;}

            /* 100percent width section with 20px padding top & bottom */
            *[class="100padtopbottom"] {width:100% !important; padding:20px 0px 20px 0px;}
        }
    </style>

    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
</head>

<body style="padding:0; margin:0" bgcolor="#687079">
    <table border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0" width="100%">
        <tr>
            <td align="center" valign="top">
                <table width="740" border="0" cellspacing="0" cellpadding="0" class="hide">
                    <tr>
                        <td height="20"></td>
                    </tr>
                </table>
<!--Company Logo-->
                <table width="740" cellspacing="0" cellpadding="20"  bgcolor="#1f242e" class="100p">
                    <tr>
                        <td align="left" width="740" valign="top" class="100p"><img src="http://digital.loggcity.dev/images/loggcity-digital-logo-long-250.png" alt="Logo" border="0" style="display:block" /></td>
                    </tr>
                </table>

                @yield('body')
<!--Social Network Section-->
                <table width="740" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" class="100p">
                    <tr>
                        <td align="center" style="font-size:16px;color:#848484;line-height:1.5">
                            <font face="'Roboto', Arial, sans-serif">
                                <span>We like to use the opportunity to quickly check in and make sure that you were able to
                                find the resource you were looking for? You might also find our social space useful for additional query and/or helpful tip:
                                </span>
                            </font>
                        </td>
                    </tr>
                </table>

                <table width="740" border="0" cellspacing="0" cellpadding="20" class="100p" bgcolor="#FFFFFF">
                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" class="100padtopbottom" width="700">
                                <tr>
                                    <td align="left" class="condensed" valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="340">
                                            <tr>
                                                <td valign="top" align="center">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td valign="top" align="center" class="100padleftright">
                                                                <table border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td width="170" align="center">
                                                                            <a href="https://facebook.com/LoggcityDigi" title="Like us on Facebook">
                                                                                <img src="http://digital.loggcity.dev/images/fb_icon.png" width="63" border="0" style="display:block;"/>
                                                                            </a>
                                                                        </td>
                                                                        <td width="170" align="center">
                                                                            <a href="https://twitter.com/LoggcityDigi" title="Follow us on Twitter">
                                                                                <img src="http://digital.loggcity.dev/images/twitter_icon.png" width="63" border="0" style="display:block;"/>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="20" class="hide"></td>
                                    <td align="left" class="condensed" valign="top">
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mob-column" width="340">
                                            <tr>
                                                <td valign="top" align="center">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td valign="top" align="center" class="100padleftright">
                                                                <table border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td width="170" align="center">
                                                                            <a href="https://www.linkedin.com/company/10301107/" title="Like on LinkedIn">
                                                                                <img src="http://digital.loggcity.dev/images/linked_icon.png" width="63" border="0" style="display:block;"/>
                                                                            </a>
                                                                        </td>
                                                                        <td width="170" align="center">
                                                                            <a href="https://www.instagram.com/loggcity" title="Check out our work on Instagram">
                                                                                <img src="http://digital.loggcity.dev/images/insta_icon.png" width="63" border="0" style="display:block;"/>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
<!--Footer Section-->
                <table width="740" border="0" cellspacing="0" cellpadding="20" bgcolor="#1f242e" class="100p">
                    <tr>
                        <td align="center" colspan="2" width="100%" style="font-size:14px; color:#cccccc;line-height:17px;">
                            <font face="'Roboto', Arial, sans-serif">
                                {{ config('app.description')}}.
                                Don't like these emails? {{ link_to_route('newsletter-subscriptions.unsubscribe', __('newsletter.email.unsubscribe'), ['email' => $email],['style'=>'color:#F96710;text-decoration: none']) }}
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" width="85%" style="font-size:13px; color:#cccccc;">
                            <font face="'Roboto', Arial, sans-serif">
                                &copy; <a style="color:#F96710;text-decoration:none" href="http://digital.loggcity.uk">Loggcity Digital</a> 2017.
                                <!--Powered by <a--  style="color:#F96710;text-decoration:none" href="http://digital.loggcity.uk">Loggcity Digital</a-->
                            </font>
                        </td>
                        <td align="right" valign="top" width="15%" style="font-size:13px; color:#cccccc;">

                        </td>
                    </tr>
                </table>
                <table width="740" class="100p" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td height="50"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
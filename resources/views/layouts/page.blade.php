<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="{{ $page->meta_description or $page['meta_description'] }}">
    <meta name="keyword" content="{{ $page->meta_keywords or $page['meta_keywords'] }}">
    <meta name="author" content="Adewale Adegoroye">  
    <meta name="google-site-verification" content="{{ setting('site.google_analytics_tracking_id') }}" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@InfoLoggcity" />
    <meta name="twitter:creator" content="@InfoLoggcity" />
    <meta property="og:title" content="{{ config('app.name', 'Loggcity Limited')  }}" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="{{ $page->meta_description  or setting('admin.description') }}" />
    <meta property="og:url" content="{{url('/')}}" />
    <meta property="og:sitename" content="{{ config('app.name', 'Loggcity Limited') }}" />
    <meta property="og:image" content="{{asset('images/lc_logo-lng.png')}}" />

    @if(isset($page->title))
        <title>@yield('page_title', $page->title. " - " . setting('admin.description'))</title>
    @else
        <title>@yield('page_title', $page['title']. " - " . setting('admin.description'))</title>
    @endif
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}" />

    <!-- Theme Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    @stack('css')
    
    <!-- Main Style -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    
    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>
<body {{$single or ''}}>
    <div id="page-wrapper">
        <header id="header">
            @include('partials._primary_menu')
            @include('partials._mobile_menu') 
        </header>
        @stack('headerbottom')
        <section id="content">
            @yield('content')
        </section>

        @include('partials.footer')
    </div>
    @include('partials._footscript')
    <!-- Scripts -->
    @stack('script')
</body>
</html>
